FROM python:3.7

RUN mkdir /app
WORKDIR /app

RUN apt-get update && pip install --upgrade pip

RUN pip install cython
RUN pip install pandas gunicorn ujson uwsgi
RUN pip install --no-binary :all: falcon
RUN pip install numba

RUN apt-get install -y libev-dev
RUN pip install bjoern

COPY ../src /app/

#CMD python json_to_numpy.py /tmp/data \
#    && python gunicorn_run.py

#CMD python gunicorn_run.py
CMD python bjoern_run.py
#CMD python bottle_run.py

#CMD gunicorn -b 0.0.0.0:80 --workers=1 main:api
#CMD uwsgi --http :80 --wsgi-file main.py --processes 4
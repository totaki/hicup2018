FROM python:2.7.15-jessie

RUN mkdir /app
WORKDIR /app

RUN apt-get update \
    && apt-get install wget gzip tar \
    && pip install --upgrade pip

RUN wget https://bitbucket.org/pypy/pypy/downloads/pypy2-v6.0.0-linux64.tar.bz2
RUN tar xvjf pypy2-v6.0.0-linux64.tar.bz2
RUN ./pypy2-v6.0.0-linux64/bin/pypy -m ensurepip
RUN ./pypy2-v6.0.0-linux64/bin/pip install cython numpy falcon pandas gunicorn ujson

COPY ../src /app/

CMD ./pypy2-v6.0.0-linux64/bin/pypy json_to_csv.py /tmp/data \
    && cat accounts.csv | wc -l \
    && ./pypy2-v6.0.0-linux64/bin/pypy gunicorn_run.py

#CMD gunicorn -b 0.0.0.0:80 --workers=1 main:api
#CMD uwsgi --http :80 --wsgi-file main.py --processes 4
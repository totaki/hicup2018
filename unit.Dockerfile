FROM python:3.7

RUN mkdir /app
WORKDIR /app

ARG NGINX_UNIT_VERSION
ENV NGINX_UNIT_VERSION 1.7
RUN echo "nginx unit version $NGINX_UNIT_VERSION"

RUN wget \
    -O nginx-unit.tar.gz \
    "https://github.com/nginx/unit/archive/$NGINX_UNIT_VERSION.tar.gz"

RUN tar -xzvf nginx-unit.tar.gz && rm nginx-unit.tar.gz

RUN cd unit-$NGINX_UNIT_VERSION \
    && ./configure \
    && ./configure python --module=py37 \
    && make all

RUN mkdir /app/modules

RUN cp unit-$NGINX_UNIT_VERSION/build/unitd /app
RUN cp unit-$NGINX_UNIT_VERSION/build/py37.unit.so /app/modules

RUN rm -Rf unit-$NGINX_UNIT_VERSION

RUN mkdir /app/control
RUN mkdir /app/state

# forward log to docker log collector
RUN ln -sf /dev/stdout /var/log/unit.log

STOPSIGNAL SIGTERM

RUN apt-get update && pip install --upgrade pip
RUN pip install \
    cython \
    pandas \
    ujson \
    numba

RUN pip install --no-binary :all: falcon
RUN pip install icc-rt

COPY ./unit.json /app/state/conf.json
COPY ./src/ /app/

COPY ./docker-entrypoint.sh ./docker-entrypoint.sh

CMD ["./docker-entrypoint.sh"]
from __future__ import unicode_literals

import os
import multiprocessing
import gunicorn.app.base

from gunicorn.six import iteritems
from main import application


def number_of_workers(max_proc):
    if max_proc:
        return (multiprocessing.cpu_count() * 2) + 1
    else:
        return 1


class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super(StandaloneApplication, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in iteritems(self.options)
                       if key in self.cfg.settings and value is not None])
        for key, value in iteritems(config):
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':
    port = os.environ.get('APP_PORT', '80')
    max_proc = os.environ.get('APP_MAX_PROC', 'false')
    options = {
        'bind': '%s:%s' % ('0.0.0.0', port),
        'workers': number_of_workers(max_proc == 'true'),
    }
    StandaloneApplication(application, options).run()

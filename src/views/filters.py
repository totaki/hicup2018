from util import Handler
from falcon.errors import HTTPBadRequest


def dummy_work(*args, **kwargs):
    pass


filters = dict()
filters['sex_eq'] = dummy_work
filters['email_domain'] = dummy_work
filters['email_lt'] = dummy_work
filters['email_gt'] = dummy_work
filters['status_eq'] = dummy_work
filters['status_neq'] = dummy_work
filters['fname_eq'] = dummy_work
filters['fname_any'] = dummy_work
filters['fname_null'] = dummy_work
filters['sname_eq'] = dummy_work
filters['sname_start'] = dummy_work
filters['sname_null'] = dummy_work
filters['phone_code'] = dummy_work
filters['phone_null'] = dummy_work
filters['country_eq'] = dummy_work
filters['country_null'] = dummy_work
filters['city_eq'] = dummy_work
filters['city_any'] = dummy_work
filters['city_null'] = dummy_work
filters['birth_lt'] = dummy_work
filters['birth_gt'] = dummy_work
filters['birth_year'] = dummy_work


class AccountsFilter(Handler):

    def on_get(self, req, resp):
        """Handles GET requests"""
        try:
            req.params.pop('query_id', None)
            limit = int(req.params.pop('limit'))
            resp.media = {"accounts": self.dl.f(req.params, limit)}
        except StopIteration:
            resp.media = {"accounts": []}
        except (KeyError, ValueError) as e:
            raise HTTPBadRequest



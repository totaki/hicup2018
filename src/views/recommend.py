from util import Handler
from falcon.status_codes import HTTP_404
from falcon.errors import HTTPBadRequest


class AccountsRecommend(Handler):
    def on_get(self, req, resp, account_id):
        try:
            req.params.pop('query_id', None)
            limit = int(req.params.pop('limit'))
            dl = self.dl
            if int(account_id) >= dl.top_id:
                resp.status = HTTP_404
            else:
                resp.media = {'accounts': []}
        except (KeyError, ValueError) as e:
            raise HTTPBadRequest

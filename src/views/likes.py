from util import Handler
from falcon.status_codes import HTTP_202, HTTP_400


class AccountsLikes(Handler):
    def on_post(self, req, resp):
        """Handles GET requests"""
        top_id = self.dl.top_id
        try:
            for i in req.media['likes']:
                if int(i['liker']) > top_id or int(i['likee']) > top_id:
                    raise ValueError
                int(i['ts'])
            resp.media = {}
            resp.status = HTTP_202
        except (KeyError, ValueError):
            resp.status = HTTP_400


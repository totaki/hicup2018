from util import Handler
from falcon.status_codes import HTTP_202, HTTP_400, HTTP_404


sex = {'m', 'f'}
status = {'свободны', 'заняты', 'всё сложно'}


class AccountsChange(Handler):
    def on_post(self, req, resp, account_id):
        """Handles GET requests"""
        dl = self.dl
        _sex = req.media.get('sex', None)
        _status = req.media.get('status', None)
        _email = req.media.get('email', None)
        if _sex and _sex not in sex:
            resp.status = HTTP_400
        elif _status and _status not in status:
            resp.status = HTTP_400
        elif _email and not dl.validate_change_email(_email):
            resp.status = HTTP_400
        elif int(account_id) >= dl.top_id:
            resp.status = HTTP_404
        else:
            resp.media = {}
            resp.status = HTTP_202

from util import Handler
from falcon.errors import HTTPBadRequest


class AccountsGroup(Handler):
    def on_get(self, req, resp):
        try:
            limit = int(req.params.pop('limit'))
            order = int(req.params.pop('order'))
            """Handles GET requests"""
            resp.media = {'groups': []}
        except (KeyError, ValueError) as e:
            raise HTTPBadRequest

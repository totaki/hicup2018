from util import Handler
from falcon.status_codes import HTTP_201, HTTP_400


class AccountsNew(Handler):
    def on_post(self, req, resp):
        """Handles GET requests"""
        dl = self.dl
        id_ = req.media.get('id', None)
        if not id_:
            resp.status = HTTP_400
        elif not dl.validate_email(req.media.get('email', None)):
            resp.status = HTTP_400
        else:
            dl.update_top_id(id_)
            resp.media = {}
            resp.status = HTTP_201


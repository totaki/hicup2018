import shutil
import argparse
import os
import time
import zipfile
import ujson
import pandas as pd
import logging
import datetime
import numpy as np
from collections import defaultdict


def load_numpy(load_path):
    with open(os.path.join(load_path, 'counts.json')) as f:
        counts = ujson.load(f)

    shape = (len(counts.keys()), max(counts.values()))
    indexer = np.zeros(shape)
    current_positions = [0 for _ in range(0, shape[0])]
    integers = np.load('./prepared/integers.npy')
    for i in range(integers.shape[0], 1, -1):
        id_, country = integers[i - 1, [0, 5]]
        pos = current_positions[country]
        indexer[country][pos] = id_
        current_positions[country] += 1
    np.save('./prepared/indexer.npy', indexer.astype(int))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--search_path', default='./prepared', required=False)
    args = parser.parse_args()
    logging.warning('Start load')
    start = time.time()
    load_numpy(args.search_path)
    logging.warning('Stop load, time %s: ', time.time() - start)

import sys
import json
import pandas as np
import numpy as np
from data_loader import Loader
import urllib.parse
from collections import defaultdict


def get_requests(scope='/accounts/filter', exclude=None):
    data = defaultdict(dict)
    with open('../temp/testing/answers/phase_1_get.answ') as f:
        for line in f:
            line = line.strip()
            if line:
                items = line.split('\t')
                query_string = items[1]
                if query_string.startswith(scope):
                    try:
                        keys = urllib.parse.parse_qs(
                            urllib.parse.urlparse(query_string).query)
                        limit = keys.pop('limit')
                        query_id = keys.pop('query_id')[0]
                        if exclude:
                            for e in exclude:
                                if e in keys:
                                    raise KeyError

                        status = items[2]
                        result = ''.join(items[3:])
                        data[len(keys)][query_id] = {
                            'status': int(status),
                            'limit': int(limit[0]),
                            'filters': {k: v[0] for k, v in keys.items()},
                            'url': query_string
                        }
                        if result:
                            data[len(keys)][query_id]['result'] = json.loads(result)[
                                'accounts']

                    except (KeyError, ValueError) as e:
                        pass
    return data


def check(func, items, args):
    with_error = []
    for k, v in items.items():
        try:
            result = func(v['filters'], v['limit'])
            if result != v['result']:
                with_error.append((v, result))
        except (KeyError, ValueError, TypeError) as e:
            if v['status'] != 400:
                with_error.append((v, e))
        except StopIteration:
            if v['result']:
                with_error.append((v, []))
        except Exception as e:
            print(v)
            raise
    print(len(with_error), '/', len(items), '\n')
    for i, j in with_error:
        if args and set(i['filters'].keys()).issuperset(args):
            print(i['url'])
            print('Expected')
            print(str(i['result']))
            print('Actual')
            print(str(j))
            print()
    print(len(with_error), '/', len(items), '\n')


requests = get_requests(
    exclude=['sname_starts', 'email_lt', 'email_gt', 'birth_lt', 'birth_gt',
             'interests_contains', 'likes_contains'])


if __name__ == '__main__':
    lvl = sys.argv[1]
    if len(sys.argv) > 2:
        args = set(sys.argv[2:])
    else:
        args = set([])
    dl = Loader('./prepared')
    dl.load()
    check(dl.f, requests[int(lvl)], args)

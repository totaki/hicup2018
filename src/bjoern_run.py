from __future__ import unicode_literals

import os
from main import application
import bjoern


if __name__ == '__main__':
    port = os.environ.get('APP_PORT', '80')
    bjoern.run(application, '0.0.0.0', int(port))

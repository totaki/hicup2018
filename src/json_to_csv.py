import shutil
import argparse
import os
import zipfile
import ujson
import pandas as pd
import logging
import datetime


ACCOUNTS_FIELDS = [
    'id',
    'email',
    'fname',
    'sname',
    'phone',
    'sex',
    'birth',
    'country',
    'city',
    'joined',
    'status',
    'prem_now',
    'premium',
    'birth_year',
    'domain',
]

INTERESTS_FIELDS = ['id', 'string']
LIKES_FIELDS = ['id', 'likes']


def load_file(f, current_ts):
    data = ujson.load(f)

    likes = []
    interests = []

    for d in data['accounts']:
        # LIKES
        current_likes = [d['id'], ';'.join(
           '%s,%s' % (i['id'], i['ts']) for i in d.pop('likes', [])
        )]
        likes.append(current_likes)

        # INTERESTS
        current_interests = [d['id'], u','.join(d.pop('interests', []))]
        interests.append(current_interests)

        # AGGREGATE
        prem = d.pop('premium', None)
        if prem:
            d['prem_now'] = prem['start'] <= current_ts <= prem['finish']
            d['premium'] = [prem['start'], prem['finish']]
        else:
            d['premium'] = None
            d['prem_now'] = False
        d['birth_year'] = datetime.datetime.fromtimestamp(d['birth']).year
        d['domain'] = d['email'].split('@')[-1]

    with open('./prepared/accounts.csv', 'a') as f:
        df = pd.DataFrame.from_dict(data['accounts'])
        df.to_csv(f, header=None, encoding='utf-8', columns=ACCOUNTS_FIELDS, index=False)

    with open('./prepared/likes.csv', 'a') as f:
        df = pd.DataFrame(likes, columns=LIKES_FIELDS)
        df.to_csv(f, header=None, columns=LIKES_FIELDS, index=False)

    with open('./prepared/interests.csv', 'a') as f:
        df = pd.DataFrame(interests, columns=INTERESTS_FIELDS)
        df.to_csv(f, header=None, encoding='utf-8', columns=INTERESTS_FIELDS, index=False)


def load(load_path, clean):
    if clean == 'true' and os.path.exists('./prepared'):
        shutil.rmtree('./prepared')

    if not os.path.exists('./prepared'):
        os.mkdir('./prepared')

    with open(os.path.join(load_path, 'options.txt')) as f:
        current_ts = int(f.readline())

    with zipfile.ZipFile(os.path.join(load_path, 'data.zip')) as f:
        filenames = filter(lambda x: x.endswith('json'), f.namelist())

        logging.warning('Loaded %s' % filenames)
        for fname in filenames:
            load_file(f.open(fname), current_ts)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('search_path')
    parser.add_argument('--clean')
    args = parser.parse_args()
    load(args.search_path, args.clean)

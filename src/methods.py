from functools import reduce
from numpy import bitwise_and, bitwise_or, uint32, zeros, array, packbits, extract, bool_
from numba import njit, prange


filters_mask = {
    'sex_eq=m': packbits([1, 0, 0, 0, 0, 0, 0, 0])[0],
    'sex_eq=f': packbits([0, 1, 0, 0, 0, 0, 0, 0])[0],
    'status_eq=свободны': packbits([0, 0, 1, 0, 0, 0, 0, 0])[0],
    'status_eq=заняты': packbits([0, 0, 0, 1, 0, 0, 0, 0])[0],
    'status_eq=всё сложно': packbits([0, 0, 0, 0, 1, 0, 0, 0])[0],
    'status_neq=свободны': packbits([0, 0, 0, 0, 0, 1, 0, 0])[0],
    'status_neq=заняты': packbits([0, 0, 0, 0, 0, 0, 1, 0])[0],
    'status_neq=всё сложно': packbits([0, 0, 0, 0, 0, 0, 0, 1])[0],
    # 'status_neq=свободны': packbits([0, 0, 0, 1, 1, 0, 0, 0])[0],
    # 'status_neq=заняты': packbits([0, 0, 1, 0, 1, 0, 0, 0])[0],
    # 'status_neq=всё сложно': packbits([0, 0, 1, 1, 0, 0, 0, 0])[0],
    # 'premium_null=1': packbits([0, 0, 0, 0, 0, 1, 0, 0])[0],
    # 'premium_null=0': packbits([0, 0, 0, 0, 0, 0, 1, 0])[0],
    # 'premium_now=1': packbits([0, 0, 0, 0, 0, 0, 0, 1])[0],
}


def get_mask(items):
    result = 0
    for k, v in list(items.items()):
        current_mask = filters_mask.get('%s=%s' % (v[-2], v[-1]), 0)
        if current_mask:
            result = bitwise_or(result, current_mask)
            items.pop(k)

    return result


def get_any(items):
    current_any = []
    for k, v in list(items.items()):
        if 'any' in v:
            return [items.pop(k)]


@njit
def filter_equal(
        buffer,
        query,
        limit,
        length,
        maximum,
        *arrays
):
    result = zeros(limit, dtype=uint32)
    position = 0

    for i in range(maximum):

        for j in range(length):
            if i >= arrays[j].shape[0]:
                continue

            current_index = arrays[j][i]
            current_buffer = buffer[current_index]

            # Check if this iteration is first
            if current_buffer[0] != query:
                current_buffer[0], current_buffer[1],  current_buffer[2] = (
                    query, 1, 0
                )
                continue

            # Check if current_index go to result
            if current_buffer[2] == 1:
                continue

            # Increment count of find
            current_buffer[1] += 1

            # Check and update buffer
            if current_buffer[1] == length:
                current_buffer[2] = 1
                result[position] = current_index
                position += 1
                if position >= limit:
                    return result

    return result


@njit
def filter_equal_one_with_mask(
        integers,
        mask,
        limit,
        maximum,
        indexes
):
    result = zeros(limit, dtype=uint32)
    position = 0

    for i in range(maximum):
        current_index = indexes[i]
        if bitwise_and(integers[current_index][-1], mask) == mask:
            result[position] = current_index
            position += 1
            if position >= limit:
                return result

    return result


def equal_one_with_mask(items, integers, limit, indexes):
    if not indexes.shape[0]:
        return indexes

    mask = reduce(bitwise_or, [filters_mask.get(i, 0) for i in items], 0)
    indexes = filter_equal_one_with_mask(
        integers, mask, limit, indexes.shape[0], indexes
    )
    return extract(indexes.astype(bool_), indexes)


@njit
def two_equal_jit(integers, index_position, number, limit, maximum, indexes):
    result = zeros(limit, dtype=uint32)
    position = 0

    for i in range(maximum):
        current_index = indexes[i]
        if integers[current_index][index_position] == number:
            result[position] = current_index
            position += 1
            if position >= limit:
                return result

    return result


@njit
def filter_equal_with_mask(
        buffer,
        query,
        integers,
        mask,
        limit,
        length,
        maximum,
        *arrays
):
    result = zeros(limit, dtype=uint32)
    position = 0

    for i in range(maximum):

        for j in range(length):
            if i >= arrays[j].shape[0]:
                continue

            current_index = arrays[j][i]
            current_buffer = buffer[current_index]

            # Check if this iteration is first
            if current_buffer[0] != query:
                current_buffer[0], current_buffer[1],  current_buffer[2] = (
                    query, 0, 0
                )

            # Check if current_index go to result
            if current_buffer[2] == 1:
                continue

            # Increment count of find
            buffer[current_index][1] += 1

            # Check and update buffer
            if current_buffer[1] == length:
                current_buffer[2] = 1
                if bitwise_and(integers[current_index][-1], mask):
                    result[position] = current_index
                    position += 1
                    if position >= limit:
                        return result

    return result

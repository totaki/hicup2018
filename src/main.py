import falcon
import logging
import os
import time
from data_loader import Loader
from views import (
    AccountsFilter,
    AccountsGroup,
    AccountsRecommend,
    AccountsSuggest,
    AccountsNew,
    AccountsChange,
    AccountsLikes
)


dl = Loader(os.environ.get('HL_CSV_DATA_PATH', './prepared'))

start_t = time.time()
dl.load()
dl.jit_prepare()
logging.warning('Loaded time %s' % (time.time() - start_t))


# GET
api = falcon.API()
api.add_route('/accounts/filter', AccountsFilter(dl))
api.add_route('/accounts/group', AccountsGroup(dl))
api.add_route('/accounts/{account_id:int}/recommend', AccountsRecommend(dl))
api.add_route('/accounts/{account_id:int}/suggest', AccountsSuggest(dl))

# POST
api.add_route('/accounts/new', AccountsNew(dl))
api.add_route('/accounts/{account_id:int}', AccountsChange(dl))
api.add_route('/accounts/likes', AccountsLikes(dl))

# Different WSGI servers try find different application name
app = api
application = api

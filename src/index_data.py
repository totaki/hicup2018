import os
import time
import ujson
import numpy as np
import logging
from numba import njit, prange
from util import Countizer, get_index

_integers_npy = None
_interests_to_user_npy = None
_emails_npy = None
_domain_json = None
_top_lvl_indexes = Countizer.load_arrays(exclude=['domain-1', 'interests-1'])
_path = './prepared'
last_pos = [0]


def get_prem(i):
    global _integers_npy
    if _integers_npy[i][8]:
        return 2
    else:
        return int(_integers_npy[i][6] > 0)


def get_domain(i):
    global _emails_npy
    _, d = _emails_npy[i].split('@')
    return _domain_json[d]


def get_neq_status_1(i):
    s = _integers_npy[i][2]
    if s == 2:
        return -1
    elif s == 3:
        return -1
    elif s == 1:
        return -2


def get_neq_status_2(i):
    s = _integers_npy[i][2]
    if s == 2:
        return -3
    elif s == 3:
        return -2
    elif s == 1:
        return -3


def get_interests(id_, last_pos, arr):
    result = np.zeros(16)
    pos = 0
    for i in prange(last_pos, 0, -1):
        if id_ == arr[i][0]:
            result[pos] = arr[i][1]
            pos += 1
            if i == 1:
                return i, result[:pos].astype(np.int32)
        else:
            return i, result[:pos].astype(np.int32)


def get_i():

    def get_interests_func():
        global _interests_to_user_npy
        last_pos[0] = _interests_to_user_npy.shape[0] - 1

        def wrapper(i):
            global _interests_to_user_npy
            slc, result = get_interests(_integers_npy[i][0], last_pos[0], _interests_to_user_npy)
            last_pos[0] = slc
            return np.extract(result.astype(np.bool), result)

        return wrapper

    return (
        ('sex', 1, _integers_npy, False),
        ('status', 2, _integers_npy, False),
        ('status', get_neq_status_1, None, False),
        ('status', get_neq_status_2, None, False),
        ('city', 12, _integers_npy, True),
        ('country', 11, _integers_npy, True),
        ('fname', 9, _integers_npy, True),
        ('sname', 10, _integers_npy, True),
        ('domain', get_domain, None, False),
        ('premium', get_prem, None, True),
        ('birth_year', 4, _integers_npy, False),
        ('phone', 14, _integers_npy, True),
        ('interests', get_interests_func(), None, False),
    )


def _indexize():
    index_positions = get_i()
    _positions1 = {i: 0 for i in _top_lvl_indexes.keys()}
    for i in range(_integers_npy.shape[0] - 1, 0, -1):
        for j in index_positions:

            if callable(j[1]):
                id_ = j[1](i)
            else:
                id_ = j[2][i][j[1]]

            if j[3] == True and id_ != 0:
                n = '%s-1' % j[0]
                pos = _positions1[n]
                _top_lvl_indexes[n][pos] = i
                _positions1[n] += 1

            if isinstance(id_, np.ndarray):
                for current_id in id_:
                    n = '%s%s' % (j[0], current_id)
                    pos = _positions1[n]
                    _top_lvl_indexes[n][pos] = i
                    _positions1[n] += 1
            else:
                n = '%s%s' % (j[0], id_)
                pos = _positions1[n]
                _top_lvl_indexes[n][pos] = i
                _positions1[n] += 1


def load():
    _globals = globals()
    file_names = list(filter(lambda x: x.split('.')[-1] in ['json', 'npy'], os.listdir(_path)))
    logging.warning('Loading data files %s', file_names)
    for fn in file_names:
        name, extension = fn.split('.')
        global_name = '_%s_%s' % (name, extension)
        if global_name in _globals:
            full_path = os.path.join(_path, fn)
            if fn.endswith('json'):
                with open(full_path) as fd:
                    _globals[global_name] = ujson.load(fd)
            elif fn.endswith('npy'):
                _globals[global_name] = np.load(full_path)

    _indexize()
    np.savez('./prepared/indexes.npz', **_top_lvl_indexes)


if __name__ == '__main__':
    start_t = time.time()
    load()
    logging.warning('Loaded time %s' % (time.time() - start_t))

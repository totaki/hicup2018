"""
Короче это данные из аналитики какие вообще бывают фильтры и как они взаимно исползуются,
так же из запросов видно что на фильтрацию нам прилетает максимум limit 40
"""

level_1 = [
    frozenset({'fname_any'}),  # skip
    frozenset({'city_any'}),  # skip
    frozenset({'interests_any'}),  # skip
    frozenset({'city_eq'}),
    frozenset({'birth_year'}),
    frozenset({'sex_eq'}),
    frozenset({'premium_null'}),
    frozenset({'status_neq'}),
    frozenset({'interests_contains'}),  # skip
    frozenset({'premium_now'}),
    frozenset({'country_eq'}),
    frozenset({'country_null'}),
    frozenset({'city_null'}),
    frozenset({'likes_contains'}),  # skip
    frozenset({'email_lt'}),  # skip
    frozenset({'phone_code'}),
    frozenset({'sname_null'}),
    frozenset({'email_gt'}),  # skip
    frozenset({'email_domain'}),
    frozenset({'birth_lt'}),  # skip
    frozenset({'fname_null'}),
    frozenset({'birth_gt'}),  # skip
    frozenset({'phone_null'}),
    frozenset({'status_eq'}),
    frozenset({'sname_starts'})  # skip
]

level_2 = [
    frozenset({'birth_lt', 'premium_now'}),   # skip
    frozenset({'country_null', 'status_eq'}),
    frozenset({'country_null', 'sex_eq'}),
    frozenset({'country_eq', 'sex_eq'}),
    frozenset({'country_eq', 'fname_any'}),   # skip
    frozenset({'interests_any', 'likes_contains'}),   # skip
    frozenset({'interests_contains', 'sex_eq'}),    # skip
    frozenset({'city_any', 'sex_eq'}),    # skip
    frozenset({'likes_contains', 'premium_now'}),    # skip
    frozenset({'interests_contains', 'status_neq'}),    # skip
    frozenset({'birth_lt', 'country_null'}),   # skip
    frozenset({'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'sex_eq'}),    # skip
    frozenset({'city_eq', 'sname_starts'}),
    frozenset({'likes_contains', 'premium_null'}),   # skip
    frozenset({'birth_year', 'premium_now'}),
    frozenset({'city_null', 'status_neq'}),
    frozenset({'sex_eq', 'sname_starts'}),   # skip
    frozenset({'fname_any', 'sex_eq'}),   # skip
    frozenset({'country_eq', 'status_neq'}),
    frozenset({'city_null', 'sex_eq'}),
    frozenset({'sex_eq', 'status_neq'}),
    frozenset({'email_domain', 'sex_eq'}),
    frozenset({'country_eq', 'interests_any'}),   # skip
    frozenset({'interests_any', 'status_eq'}),   # skip
    frozenset({'email_lt', 'sex_eq'}),   # skip
    frozenset({'birth_gt', 'city_eq'}),   # skip
    frozenset({'interests_contains', 'likes_contains'}),   # skip
    frozenset({'city_eq', 'email_lt'}),    # skip
    frozenset({'birth_lt', 'status_neq'}),    # skip
    frozenset({'country_eq', 'email_domain'}),
    frozenset({'birth_lt', 'sex_eq'}),   # skip
    frozenset({'country_null', 'email_gt'}),    # skip
    frozenset({'city_null', 'email_lt'}),   # skip
    frozenset({'city_eq', 'status_eq'}),
    frozenset({'birth_lt', 'likes_contains'}),   # skip
    frozenset({'country_null', 'fname_any'}),    # skip
    frozenset({'likes_contains', 'status_eq'}),   # skip
    frozenset({'birth_year', 'sex_eq'}),
    frozenset({'country_eq', 'email_gt'}),  # skip
    frozenset({'city_null', 'status_eq'}),
    frozenset({'birth_year', 'likes_contains'}),   # skip
    frozenset({'birth_lt', 'city_any'}),   # skip
    frozenset({'interests_any', 'sex_eq'}),   # skip
    frozenset({'birth_gt', 'status_neq'}),   # skip
    frozenset({'country_eq', 'interests_contains'}),   # skip
    frozenset({'likes_contains', 'status_neq'}),   # skip
    frozenset({'country_eq', 'sname_null'}),
    frozenset({'premium_null', 'sex_eq'}),
    frozenset({'city_null', 'email_gt'}),   # skip
    frozenset({'interests_any', 'status_neq'}),
    frozenset({'city_any', 'phone_null'}),   # skip
    frozenset({'city_eq', 'sex_eq'}),
    frozenset({'city_any', 'status_neq'}),   # skip
    frozenset({'city_null', 'sname_starts'}),   # skip
    frozenset({'phone_code', 'sex_eq'}),
    frozenset({'email_gt', 'sex_eq'}),    # skip
    frozenset({'sex_eq', 'sname_null'}),
    frozenset({'city_eq', 'email_gt'}),   # skip
    frozenset({'city_any', 'fname_any'}),    # skip
    frozenset({'country_null', 'fname_null'}),
    frozenset({'interests_contains', 'status_eq'}),   # skip
    frozenset({'country_null', 'phone_null'}),
    frozenset({'country_eq', 'sname_starts'}),   # skip
    frozenset({'interests_contains', 'premium_now'}),   # skip
    frozenset({'birth_year', 'country_eq'}),
    frozenset({'fname_null', 'sex_eq'}),
    frozenset({'city_eq', 'email_domain'}),
    frozenset({'country_null', 'status_neq'}),
    frozenset({'birth_gt', 'likes_contains'}),   # skip
    frozenset({'city_null', 'interests_contains'}),   # skip
    frozenset({'interests_contains', 'premium_null'}),   # skip
    frozenset({'birth_lt', 'status_eq'}),   # skip
    frozenset({'city_any', 'email_gt'}),  # skip
    frozenset({'city_null', 'fname_any'}),  # skip
    frozenset({'birth_year', 'status_eq'}),
    frozenset({'interests_any', 'premium_now'}),   # skip
    frozenset({'country_null', 'sname_starts'}),   # skip
    frozenset({'birth_gt', 'country_eq'}),   # skip
    frozenset({'birth_gt', 'status_eq'}),   # skip
    frozenset({'country_null', 'email_lt'}),   # skip
    frozenset({'premium_now', 'sex_eq'}),
    frozenset({'city_null', 'interests_any'}),   # skip
    frozenset({'birth_gt', 'country_null'}),   # skip
    frozenset({'country_eq', 'fname_null'}),
    frozenset({'city_any', 'interests_contains'}),   # skip
    frozenset({'country_eq', 'phone_code'}),
    frozenset({'city_eq', 'phone_code'}),
    frozenset({'city_null', 'fname_null'}),
    frozenset({'country_eq', 'phone_null'}),
    frozenset({'birth_year', 'city_any'}),
    frozenset({'birth_gt', 'city_any'}),    # skip
    frozenset({'city_eq', 'status_neq'}),
    frozenset({'city_any', 'sname_null'}),    # skip
    frozenset({'birth_year', 'status_neq'}),
    frozenset({'country_eq', 'email_lt'}),   # skip
    frozenset({'country_eq', 'status_eq'}),
    frozenset({'city_any', 'sname_starts'}),   # skip
    frozenset({'city_eq', 'sname_null'}),
    frozenset({'phone_null', 'sex_eq'}),
    frozenset({'city_any', 'fname_null'}),    # skip
    frozenset({'city_any', 'interests_any'}),    # skip
    frozenset({'country_null', 'interests_any'}),   # skip
    frozenset({'city_any', 'email_domain'}),   # skip
    frozenset({'interests_any', 'premium_null'}),   # skip
    frozenset({'country_null', 'sname_null'}),
    frozenset({'birth_lt', 'city_eq'}),  # skip
    frozenset({'birth_year', 'country_null'}),
    frozenset({'country_null', 'interests_contains'}),   # skip
    frozenset({'city_null', 'phone_null'}),
    frozenset({'birth_year', 'premium_null'}),
    frozenset({'city_any', 'email_lt'}),  # skip
    frozenset({'birth_lt', 'country_eq'}),   # skip
    frozenset({'birth_gt', 'premium_now'}),   # skip
    frozenset({'birth_gt', 'premium_null'}),   # skip
    frozenset({'city_null', 'phone_code'}),
    frozenset({'country_null', 'email_domain'}),
    frozenset({'birth_year', 'city_eq'}),
    frozenset({'birth_year', 'city_null'}),
    frozenset({'birth_lt', 'city_null'}),    # skip
    frozenset({'birth_lt', 'premium_null'}),    # skip
    frozenset({'city_any', 'phone_code'}),   # skip
    frozenset({'city_eq', 'fname_any'}),   # skip
    frozenset({'city_null', 'email_domain'}),
    frozenset({'city_eq', 'fname_null'}),
    frozenset({'country_null', 'phone_code'}),
    frozenset({'birth_gt', 'city_null'}),   # skip
    frozenset({'city_null', 'sname_null'}),
    frozenset({'city_eq', 'phone_null'}),
    frozenset({'city_eq', 'interests_any'}),    # skip
    frozenset({'city_any', 'status_eq'}),   # skip
    frozenset({'city_eq', 'interests_contains'})   # skip
]

level_3 = [
    frozenset({'birth_lt', 'likes_contains', 'status_neq'}),
    frozenset({'birth_year', 'likes_contains', 'status_eq'}),
    frozenset({'city_any', 'sex_eq', 'status_neq'}),
    frozenset({'country_eq', 'email_gt', 'sex_eq'}),
    frozenset({'city_any', 'fname_any', 'sex_eq'}),
    frozenset({'country_eq', 'fname_any', 'sex_eq'}),
    frozenset({'country_null', 'email_domain', 'sex_eq'}),
    frozenset({'country_null', 'email_lt', 'sex_eq'}),
    frozenset({'birth_gt', 'country_eq', 'sex_eq'}),
    frozenset({'country_null', 'interests_any', 'sex_eq'}),
    frozenset({'city_eq', 'sex_eq', 'sname_null'}),
    frozenset({'interests_any', 'premium_now', 'sex_eq'}),
    frozenset({'country_eq', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'email_gt', 'sex_eq'}),
    frozenset({'country_eq', 'email_domain', 'sex_eq'}),
    frozenset({'city_any', 'fname_null', 'sex_eq'}),
    frozenset({'city_eq', 'email_gt', 'sex_eq'}),
    frozenset({'birth_year', 'premium_null', 'sex_eq'}),
    frozenset({'birth_lt', 'likes_contains', 'premium_now'}),
    frozenset({'city_null', 'interests_contains', 'sex_eq'}),
    frozenset({'birth_gt', 'sex_eq', 'status_eq'}),
    frozenset({'country_eq', 'fname_null', 'sex_eq'}),
    frozenset({'birth_lt', 'city_eq', 'sex_eq'}),
    frozenset({'birth_gt', 'likes_contains', 'status_eq'}),
    frozenset({'country_eq', 'phone_code', 'sex_eq'}),
    frozenset({'interests_any', 'premium_null', 'sex_eq'}),
    frozenset({'country_null', 'interests_contains', 'sex_eq'}),
    frozenset({'city_any', 'email_lt', 'sex_eq'}),
    frozenset({'country_eq', 'interests_contains', 'sex_eq'}),
    frozenset({'city_eq', 'interests_any', 'sex_eq'}),
    frozenset({'birth_year', 'country_eq', 'sex_eq'}),
    frozenset({'city_null', 'interests_contains', 'status_neq'}),
    frozenset({'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'city_eq', 'email_lt', 'sex_eq'}),
    frozenset({'city_any', 'interests_any', 'sex_eq'}),
    frozenset({'birth_lt', 'sex_eq', 'status_neq'}),
    frozenset({'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'sex_eq', 'status_eq'}),
    frozenset({'city_eq', 'sex_eq', 'status_neq'}),
    frozenset({'interests_contains', 'likes_contains', 'status_eq'}),
    frozenset({'interests_any', 'likes_contains', 'premium_null'}),
    frozenset({'birth_gt', 'likes_contains', 'status_neq'}),
    frozenset({'city_null', 'sex_eq', 'status_eq'}),
    frozenset({'country_null', 'interests_contains', 'status_neq'}),
    frozenset({'city_any', 'phone_null', 'sex_eq'}),
    frozenset({'birth_gt', 'premium_null', 'sex_eq'}),
    frozenset({'birth_lt', 'likes_contains', 'status_eq'}),
    frozenset({'interests_contains', 'premium_null', 'sex_eq'}),
    frozenset({'birth_gt', 'sex_eq', 'status_neq'}),
    frozenset({'city_null', 'email_gt', 'sex_eq'}),
    frozenset({'country_eq', 'interests_any', 'sex_eq'}),
    frozenset({'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'country_eq', 'status_neq'}),
    frozenset({'country_eq', 'sex_eq', 'status_eq'}),
    frozenset({'country_eq', 'phone_null', 'sex_eq'}),
    frozenset({'city_any', 'sex_eq', 'sname_starts'}),
    frozenset({'city_any', 'sex_eq', 'sname_null'}),
    frozenset({'city_null', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'interests_contains', 'sex_eq'}),
    frozenset({'country_null', 'phone_null', 'sex_eq'}),
    frozenset({'interests_any', 'likes_contains', 'premium_now'}),
    frozenset({'birth_year', 'city_null', 'sex_eq'}),
    frozenset({'birth_lt', 'city_eq', 'status_neq'}),
    frozenset({'interests_contains', 'likes_contains', 'premium_null'}),
    frozenset({'city_any', 'email_domain', 'sex_eq'}),
    frozenset({'country_null', 'phone_code', 'sex_eq'}),
    frozenset({'country_null', 'sex_eq', 'sname_null'}),
    frozenset({'birth_gt', 'country_null', 'sex_eq'}),
    frozenset({'city_eq', 'sex_eq', 'status_eq'}),
    frozenset({'city_eq', 'email_domain', 'sex_eq'}),
    frozenset({'birth_gt', 'city_null', 'sex_eq'}),
    frozenset({'birth_lt', 'premium_null', 'sex_eq'}),
    frozenset({'city_eq', 'interests_contains', 'sex_eq'}),
    frozenset({'birth_lt', 'premium_now', 'sex_eq'}),
    frozenset({'country_eq', 'email_lt', 'sex_eq'}),
    frozenset({'country_null', 'email_gt', 'sex_eq'}),
    frozenset({'city_null', 'email_domain', 'sex_eq'}),
    frozenset({'country_null', 'sex_eq', 'status_neq'}),
    frozenset({'interests_any', 'likes_contains', 'status_neq'}),
    frozenset({'birth_lt', 'country_null', 'sex_eq'}),
    frozenset({'interests_contains', 'premium_now', 'sex_eq'}),
    frozenset({'birth_year', 'likes_contains', 'premium_now'}),
    frozenset({'country_eq', 'sex_eq', 'sname_starts'}),
    frozenset({'city_any', 'interests_contains', 'status_neq'}),
    frozenset({'city_null', 'sex_eq', 'sname_null'}),
    frozenset({'country_null', 'sex_eq', 'sname_starts'}),
    frozenset({'birth_lt', 'sex_eq', 'status_eq'}),
    frozenset({'city_any', 'interests_any', 'status_eq'}),
    frozenset({'country_null', 'interests_contains', 'status_eq'}),
    frozenset({'city_eq', 'phone_null', 'sex_eq'}),
    frozenset({'interests_contains', 'likes_contains', 'status_neq'}),
    frozenset({'city_eq', 'fname_any', 'sex_eq'}),
    frozenset({'city_null', 'phone_code', 'sex_eq'}),
    frozenset({'country_eq', 'interests_contains', 'status_eq'}),
    frozenset({'city_null', 'sex_eq', 'sname_starts'}),
    frozenset({'city_eq', 'interests_contains', 'status_neq'}),
    frozenset({'city_eq', 'sex_eq', 'sname_starts'}),
    frozenset({'city_eq', 'fname_null', 'sex_eq'}),
    frozenset({'city_any', 'interests_contains', 'status_eq'}),
    frozenset({'birth_year', 'likes_contains', 'premium_null'}),
    frozenset({'country_eq', 'interests_contains', 'status_neq'}),
    frozenset({'country_eq', 'sex_eq', 'sname_null'}),
    frozenset({'interests_contains', 'likes_contains', 'premium_now'}),
    frozenset({'birth_gt', 'city_any', 'sex_eq'}),
    frozenset({'city_eq', 'interests_any', 'status_neq'}),
    frozenset({'birth_lt', 'country_eq', 'sex_eq'}),
    frozenset({'country_null', 'sex_eq', 'status_eq'}),
    frozenset({'city_null', 'fname_null', 'sex_eq'}),
    frozenset({'birth_gt', 'premium_now', 'sex_eq'}),
    frozenset({'city_null', 'email_lt', 'sex_eq'}),
    frozenset({'birth_year', 'country_null', 'sex_eq'}),
    frozenset({'country_null', 'fname_any', 'sex_eq'}),
    frozenset({'country_eq', 'interests_any', 'status_neq'}),
    frozenset({'birth_year', 'likes_contains', 'status_neq'}),
    frozenset({'interests_any', 'likes_contains', 'status_eq'}),
    frozenset({'birth_gt', 'likes_contains', 'premium_null'}),
    frozenset({'country_eq', 'interests_any', 'status_eq'}),
    frozenset({'birth_year', 'premium_now', 'sex_eq'}),
    frozenset({'birth_year', 'country_null', 'status_eq'}),
    frozenset({'city_null', 'interests_any', 'sex_eq'}),
    frozenset({'country_null', 'interests_any', 'status_eq'}),
    frozenset({'birth_lt', 'city_eq', 'status_eq'}),
    frozenset({'birth_year', 'country_eq', 'status_eq'}),
    frozenset({'birth_gt', 'city_eq', 'sex_eq'}),
    frozenset({'country_null', 'fname_null', 'sex_eq'}),
    frozenset({'country_null', 'interests_any', 'status_neq'}),
    frozenset({'city_any', 'phone_code', 'sex_eq'}),
    frozenset({'city_eq', 'phone_code', 'sex_eq'}),
    frozenset({'city_null', 'fname_any', 'sex_eq'}),
    frozenset({'birth_year', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_any', 'sex_eq'}),
    frozenset({'birth_year', 'city_eq', 'sex_eq'}),
    frozenset({'city_eq', 'interests_any', 'status_eq'}),
    frozenset({'birth_year', 'city_any', 'sex_eq'}),
    frozenset({'birth_lt', 'country_null', 'status_eq'}),
    frozenset({'birth_gt', 'city_any', 'status_neq'}),
    frozenset({'city_any', 'interests_any', 'status_neq'}),
    frozenset({'city_null', 'phone_null', 'sex_eq'}),
    frozenset({'city_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'likes_contains', 'premium_null'}),
    frozenset({'birth_lt', 'country_null', 'status_neq'}),
    frozenset({'birth_lt', 'city_null', 'status_neq'}),
    frozenset({'birth_gt', 'country_null', 'status_neq'}),
    frozenset({'birth_lt', 'country_eq', 'status_eq'}),
    frozenset({'birth_gt', 'likes_contains', 'premium_now'}),
    frozenset({'birth_year', 'city_any', 'status_eq'}),
    frozenset({'city_null', 'interests_contains', 'status_eq'}),
    frozenset({'birth_year', 'city_eq', 'status_eq'}),
    frozenset({'city_null', 'interests_any', 'status_eq'}),
    frozenset({'birth_gt', 'city_eq', 'status_eq'}),
    frozenset({'birth_gt', 'country_eq', 'status_eq'}),
    frozenset({'birth_lt', 'city_null', 'sex_eq'}),
    frozenset({'birth_lt', 'city_null', 'status_eq'}),
    frozenset({'city_eq', 'interests_contains', 'status_eq'}),
    frozenset({'birth_year', 'country_null', 'status_neq'}),
    frozenset({'birth_gt', 'country_null', 'status_eq'}),
    frozenset({'birth_year', 'country_eq', 'status_neq'}),
    frozenset({'birth_gt', 'city_eq', 'status_neq'}),
    frozenset({'birth_gt', 'country_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_any', 'status_neq'}),
    frozenset({'birth_year', 'city_eq', 'status_neq'}),
    frozenset({'birth_year', 'city_null', 'status_eq'}),
    frozenset({'birth_year', 'city_null', 'status_neq'}),
    frozenset({'city_null', 'interests_any', 'status_neq'}),
    frozenset({'birth_year', 'city_any', 'status_neq'}),
    frozenset({'birth_gt', 'city_any', 'status_eq'}),
    frozenset({'birth_lt', 'city_any', 'status_eq'})
]

level_4 = [
    frozenset({'country_null', 'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'city_eq', 'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'country_eq', 'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'country_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_gt', 'city_eq', 'sex_eq', 'status_eq'}),
    frozenset({'birth_year', 'country_eq', 'sex_eq', 'status_neq'}),
    frozenset({'country_null', 'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'city_null', 'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'birth_gt', 'country_eq', 'sex_eq', 'status_eq'}),
    frozenset({'city_null', 'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_year', 'city_null', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'country_null', 'sex_eq', 'status_eq'}),
    frozenset({'country_eq', 'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'country_eq', 'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_year', 'country_eq', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'country_null', 'sex_eq', 'status_eq'}),
    frozenset({'city_null', 'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'country_null', 'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'country_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'country_eq', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'country_eq', 'sex_eq', 'status_neq'}),
    frozenset({'city_eq', 'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'country_eq', 'interests_any', 'sex_eq', 'status_eq'}),
    frozenset({'country_null', 'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'city_any', 'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'city_eq', 'interests_contains', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_eq', 'sex_eq', 'status_neq'}),
    frozenset({'birth_gt', 'city_null', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'city_any', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'city_any', 'sex_eq', 'status_neq'}),
    frozenset({'city_null', 'interests_any', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'country_null', 'sex_eq', 'status_eq'}),
    frozenset({'birth_year', 'city_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_gt', 'city_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'city_eq', 'sex_eq', 'status_neq'}),
    frozenset({'city_eq', 'interests_contains', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'country_eq', 'sex_eq', 'status_eq'}),
    frozenset({'birth_gt', 'city_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'country_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'city_eq', 'sex_eq', 'status_neq'}),
    frozenset({'birth_year', 'city_eq', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'city_null', 'sex_eq', 'status_eq'}),
    frozenset({'birth_year', 'city_any', 'sex_eq', 'status_eq'}),
    frozenset({'birth_lt', 'city_any', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_null', 'sex_eq', 'status_neq'}),
    frozenset({'birth_lt', 'city_eq', 'sex_eq', 'status_eq'})
]

import argparse
import logging
import time
import json
from util import read_data

data = {
    'accounts_count': 0,
    'max_account_id': 0,
    'interests_count': 0,
    'max_interests': 0,
    'max_likes': 0,
    'likes_count': 0,
    'max_joined_delta_like': 0,
    'max_current_delta_like': 0
}


def parse_file(accounts, ts, *args):
    for i in accounts:
        data['accounts_count'] += 1
        data['max_account_id'] = max(data['max_account_id'], i['id'])
        joined = i['joined']
        interests = i.get('interests', [])
        likes = i.get('likes', [])
        data['interests_count'] += len(interests)
        data['likes_count'] += len(likes)
        data['max_interests'] = max(data['max_interests'], len(interests))
        data['max_likes'] = max(data['max_likes'], len(likes))
        for j in likes:
            data['max_joined_delta_like'] = max(data['max_joined_delta_like'], j['ts'] - joined)
            data['max_current_delta_like'] = max(data['max_current_delta_like'], j['ts'] - joined)


def load(data_path, clean):
    read_data(data_path, parse_file)
    with open('./prepared/data_stats_json', 'w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path')
    parser.add_argument('--clean')
    args = parser.parse_args()
    logging.warning('Start load')
    start = time.time()
    load(args.data_path, args.clean)
    logging.warning('Stop load, time %s: ', time.time() - start)

import shutil
import argparse
import os
import time
import zipfile
import ujson
import pandas as pd
import logging
import datetime
import numpy as np
from collections import defaultdict


ACCOUNTS_FIELDS = [
    'id',
    'email',
    'fname',
    'sname',
    'phone',
    'sex',
    'birth',
    'country',
    'city',
    'joined',
    'status',
    'premium',
]

to_sex = {
    'm': 1,
    'f': 2
}

from_sex = ('m', 'f')


to_status = {
    'свободны': 1,
    'заняты': 2,
    'всё сложно': 3
}


class StringIndexer:

    def __init__(self):
        self.current = 1
        self.dct = {'': 0}
        self.counts = defaultdict(int)

    def get_index(self, string):
        self.counts[string] += 1
        try:
            return self.dct[string]
        except KeyError:
            self.dct[string] = self.current
            result = self.current
            self.current += 1
            return result


indexer = StringIndexer()


def load_file(f, current_ts):
    data = ujson.load(f)

    results = np.array([
        [
            to_sex[i['sex']],
            to_status[i['status']],
        ]
        for i in data['accounts']
    ])

    with open('./prepared/accounts.csv', 'a') as f:
        df = pd.DataFrame.from_dict(data['accounts'])
        df.to_csv(f, header=None, encoding='utf-8', columns=ACCOUNTS_FIELDS, index=False)

    return results


def load_file_numpy(f, current_ts):
    data = ujson.load(f)

    int_result = np.array([
        [
            i['id'],
            ord(i['sex']),
            to_status[i['status']],
            i['birth'],
            i['joined'],
            indexer.get_index(i.get('country', ''))
        ]
        for i in data['accounts']
    ])

    str_result = np.array([
        [
            i['email'],
            i.get('fname', ''),
            i.get('sname', ''),
            i.get('phone', ''),
            i.get('country', ''),
            i.get('city', ''),
            i.get('status', ''),
        ]
        for i in data['accounts']
    ])

    return int_result, str_result


def load(load_path, clean):
    if clean == 'true' and os.path.exists('./prepared'):
        shutil.rmtree('./prepared')

    if not os.path.exists('./prepared'):
        os.mkdir('./prepared')

    with open(os.path.join(load_path, 'options.txt')) as f:
        current_ts = int(f.readline())

    result_integers_array = np.zeros(2)

    with zipfile.ZipFile(os.path.join(load_path, 'data.zip')) as f:
        filenames = list(filter(lambda x: x.endswith('json'), f.namelist()))

        logging.warning('Loaded %s' % filenames)
        for fname in filenames:
            integers_array = load_file(f.open(fname), current_ts)
            result_integers_array = np.vstack((result_integers_array, integers_array))

    np.save('./prepared/integers.npy', result_integers_array.astype(int))


def load_numpy(load_path, clean):
    if clean == 'true' and os.path.exists('./prepared'):
        shutil.rmtree('./prepared')

    if not os.path.exists('./prepared'):
        os.mkdir('./prepared')

    with open(os.path.join(load_path, 'options.txt')) as f:
        current_ts = int(f.readline())

    result_integers_array = np.zeros(6)
    result_strings_array = np.zeros(7)

    with zipfile.ZipFile(os.path.join(load_path, 'data.zip')) as f:
        filenames = list(filter(lambda x: x.endswith('json'), f.namelist()))

        logging.warning('Loaded %s' % filenames)
        for fname in filenames:
            integers_array, strings_array = load_file_numpy(f.open(fname), current_ts)
            result_integers_array = np.vstack((result_integers_array, integers_array))
            result_strings_array = np.vstack((result_strings_array, strings_array))

    logging.warning('Size of array %s' % result_integers_array.shape[0])
    np.save('./prepared/integers.npy', result_integers_array.astype(int))
    np.save('./prepared/strings.npy', result_strings_array)
    with open('./prepared/strings.json', 'w') as f:
        logging.warning('Total unique strings %s' % (indexer.current - 1))
        ujson.dump(indexer.dct, f)

    with open('./prepared/counts.json', 'w') as f:
        logging.warning('Check indexes sum %s' % sum(indexer.counts.values()))
        ujson.dump(indexer.counts, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('search_path')
    parser.add_argument('--clean')
    args = parser.parse_args()
    logging.warning('Start load')
    start = time.time()
    load_numpy(args.search_path, args.clean)
    logging.warning('Stop load, time %s: ', time.time() - start)

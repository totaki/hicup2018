import os
import shutil
import argparse
import logging
import numpy as np
import time
from util import read_data, StringIndexer, Countizer
from datetime import datetime

countizer = Countizer()


fname_indexer = StringIndexer('fname', countizer)
sname_indexer = StringIndexer('sname', countizer)
country_indexer = StringIndexer('country', countizer)
city_indexer = StringIndexer('city', countizer)
interests_indexer = StringIndexer('interests', countizer)
domain_indexer = StringIndexer('domain', countizer)
# TODO: тут надо сделать с запасом появляется где-то 25к новых аккаунтов

data_for_save = {
    'created': None,
    'last_position': 1
}

integers = [[0 for i in range(15)]]
emails = [0]
interests_to_user = []
likes_to_user = []


to_status = {
    'свободны': 1,
    'заняты': 2,
    'всё сложно': 3
}


def get_phone_fields(phone):
    """
    :param phone:
    :return: номер, код
    """
    if phone:
        i, k = phone.split(')')
        _, j = i.split('(')
        countizer.update_count('phone', int(j))
        countizer.update_count('phone', -1)
        return [int(k), int(j)]
    else:
        countizer.update_count('phone', 0)
        return [0, 0]


def get_premium_fields(ts, start, finish):
    not_null = 1 if start and finish else 0
    now = start <= ts <= finish
    if not_null:
        countizer.update_count('premium', -1)
        if now:
            countizer.update_count('premium', 2)
        else:
            countizer.update_count('premium', 1)
    else:
        countizer.update_count('premium', 0)
    return start, finish, int(now)


def get_sex(i):
    s = ord(i['sex'])
    countizer.update_count('sex', s)
    return s


def get_status(i):
    s = to_status[i['status']]
    countizer.update_count('status', s)
    if s == 2:
        countizer.update_count('status', -1)
        countizer.update_count('status', -3)
    elif s == 3:
        countizer.update_count('status', -1)
        countizer.update_count('status', -2)
    elif s == 1:
        countizer.update_count('status', -2)
        countizer.update_count('status', -3)
    return s


def get_year(i):
    year = datetime.fromtimestamp(i['birth']).year
    countizer.update_count('birth_year', year)
    return year


def get_email(i):
    _, domain = i['email'].split('@')
    domain_indexer.get_index(domain)
    return i['email']


def get_map(sex, status, premium_start, premium_finish, ts):
    return np.packbits([
        sex == 'm',
        sex == 'f',
        status == 'свободны',
        status == 'заняты',
        status == 'всё сложно',
        status != 'свободны',
        status != 'заняты',
        status != 'всё сложно',
        # premium_start == '0',
        # premium_start != '0',
        # premium_start <= ts <= premium_finish
    ])[0]


def parse_file(accounts, ts, last_id: int):
    if not data_for_save['created']:
        data_for_save['integers'] = np.zeros((last_id + 1, 16))
        data_for_save['emails'] = ['']
        data_for_save['created'] = True

    for i in accounts:
        # Integers
        current = data_for_save['integers'][data_for_save['last_position']]
        current[0] = i['id']
        current[1] = get_sex(i)
        current[2] = get_status(i)
        current[3] = i['birth']
        current[4] = get_year(i)
        current[5] = i['joined']
        current[6], current[7], current[8] = get_premium_fields(
            ts,
            **i.get('premium', {'start': 0, 'finish': 0})
        )
        current[9] = fname_indexer.get_index(i.get('fname', ''))
        current[10] = sname_indexer.get_index(i.get('sname', ''))
        current[11] = country_indexer.get_index(i.get('country', ''))
        current[12] = city_indexer.get_index(i.get('city', ''))
        current[13], current[14] = get_phone_fields(i.get('phone', None))
        current[15] = get_map(
            i['sex'],
            i['status'],
            current[6],
            current[7],
            ts
        )

        # Emails
        data_for_save['emails'].append(get_email(i))

        # After
        data_for_save['last_position'] += 1

    for i in accounts:
        interest = i.get('interests', [])
        if interest:
            interests_to_user.extend([
                [i['id'], interests_indexer.get_index(k)]
                for k in interest
            ])


def load(data_path, clean):
    if clean == 'true' and os.path.exists('./prepared'):
        shutil.rmtree('./prepared')

    if not os.path.exists('./prepared'):
        os.mkdir('./prepared')

    read_data(data_path, parse_file)
    fname_indexer.save()
    sname_indexer.save()
    country_indexer.save()
    city_indexer.save()
    interests_indexer.save()
    domain_indexer.save()
    countizer.save()
    np.save('./prepared/integers.npy', np.array(data_for_save['integers']).astype(np.uint32))
    np.save('./prepared/emails.npy', data_for_save['emails'])
    np.save('./prepared/interests_to_user.npy', np.array(interests_to_user).astype(int))
    np.save('./prepared/likes_to_user.npy', np.array(likes_to_user).astype(int))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('data_path')
    parser.add_argument('--clean')
    args = parser.parse_args()
    logging.warning('Start load')
    start = time.time()
    load(args.data_path, args.clean)
    logging.warning('Stop load, time %s: ', time.time() - start)

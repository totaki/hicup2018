# -*- coding: utf-8 -*-


def sex(v):
    if v in {'m', 'f'}:
        return v
    else:
        raise ValueError


def email(v):
    return v


def status(v):
    if v in {u'свободны', u'заняты', u'всё сложно'}:
        return v.encode('utf-8')
    else:
        raise ValueError


def fname(v):
    if len(v) <= 50:
        return v.encode('utf-8')
    else:
        raise ValueError


def sname(v):
    if len(v) <= 50:
        return v.encode('utf-8')
    else:
        raise ValueError


def phone(v):
    return v


def birth(v):
    return int(v)


def country(v):
    if len(v) <= 50:
        return v.encode('utf-8')
    else:
        raise ValueError


def city(v):
    if len(v) <= 50:
        return v.encode('utf-8')
    else:
        raise ValueError


def interests(v):
    return v


def line(v):
    return v


def premium(v):
    return v

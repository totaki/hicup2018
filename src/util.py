from collections import defaultdict

import numpy as np
import logging
import os
import ujson
import zipfile
from numba import njit, prange


class Handler(object):
    def __init__(self, dl):
        """
        :param dl: data loader instance
        """
        self.dl = dl


class Countizer:

    def __init__(self, load_path='./prepared'):
        self.load_path = load_path
        self.counts = defaultdict(lambda: defaultdict(int))
        self.all_counts = 0

    def update_count(self, base, index_value):
        self.all_counts += 1
        self.counts[base][index_value] += 1

    def save(self):
        logging.warning('Total counts for store %s' % (self.all_counts,))
        with open(os.path.join(self.load_path, 'countizer.json'), 'w') as f:
            ujson.dump(self.counts, f)

    @classmethod
    def load_arrays(cls, load_path='./prepared', exclude=None):
        arrays = {}
        with open(os.path.join(load_path, 'countizer.json')) as f:
            d = ujson.load(f)
            for k, v in d.items():
                for k1, v1 in v.items():
                    key = '%s%s' % (k, k1)
                    if exclude and key not in exclude:
                        arrays[key] = np.zeros(v1, dtype=np.uint32)
        return arrays


class StringIndexer:

    def __init__(self, base_name, countizer, load_path='./prepared'):
        self.base_name = base_name
        self.load_path = load_path
        self.countizer = countizer
        self.dct = {'': 0, '__next__': 1}

    def get_index(self, string):
        try:
            result = self.dct[string]
        except KeyError:
            self.dct[string] = self.dct['__next__']
            result = self.dct['__next__']
            self.dct['__next__'] += 1

        if result != 0:
            self.countizer.update_count(self.base_name, -1)
        self.countizer.update_count(self.base_name, result)
        return result

    def save(self):
        logging.warning('Total unique strings %s for %s' % (self.dct['__next__'] - 1, self.base_name))
        with open(os.path.join(self.load_path, self.base_name + '.json'), 'w') as f:
            ujson.dump(self.dct, f)
        arr = sorted(self.dct.keys(), key=lambda x: self.dct[x])
        np.save(os.path.join(self.load_path, self.base_name + '.npy'), arr)

    @staticmethod
    def load(base_name):
        with open(os.path.join('./prepared', base_name)) as f:
            return ujson.load(f)


def read_data(data_path, chunk_callback):
    with open(os.path.join(data_path, 'options.txt')) as f:
        current_ts = int(f.readline())

    with zipfile.ZipFile(os.path.join(data_path, 'data.zip')) as f:
        filenames = list(filter(lambda x: x.endswith('json'), f.namelist()))
        filenames = sorted(filenames, key=lambda x: int(x.split('_')[-1].split('.')[0]))

        last_id = ujson.load(f.open(filenames[-1]))['accounts'][-1]['id']
        logging.warning('Loaded %s' % filenames)
        for filename in filenames:
            data = ujson.load(f.open(filename))
            chunk_callback(data['accounts'], current_ts, last_id)
            del data


def get_names(com_func, val_func):
    field_name = val_func.__name__
    query_name = '%s_%s' % (com_func.__name__, field_name)
    return field_name, query_name


def get_field(com_func, val_func, extractor=None, after=None):
    """
    Вообщем как это гавно должно работать, первым делом мы прогоняем нужное нам поле через
    валидаров. В случае если что не так ловим ValueError и забиваем. Если все ок, мы должны получить
    название поля котороые будем выдергивать из нашего DataFrame или extractor, которые нам выдернит
    нужное поле для сравнения. Вообще поидеи всегда можно передавать extractor. Далее нам так же
    нужна функция сравнения и функция если определена, которая может как-то пост обработать нашу запись.
    Кажется это должно покрайней мере стать удобным инструментом, по скорости пока ничего не скажу.
    :param com_func: функция
    :param val_func: функция
    :param extractor: функция
    :param after: функция
    :return:
    """
    field_name, query_name = get_names(com_func, val_func)
    return (query_name, val_func), \
           (field_name, (com_func, extractor, after))


available_filters = (
    ('sex', ('eq', )),
    ('email', ('domain', 'lt', 'gt')),
    ('status', ('eq', 'neq')),
    ('fname', ('eq', 'any', 'null')),
    ('sname', ('eq', 'starts', 'null')),
    ('phone', ('code', 'null')),
    ('country', ('eq', 'null')),
    ('city', ('eq', 'any', 'null')),
    ('birth', ('lt', 'gt', 'year')),
    ('interests', ('contains', 'any')),
    ('likes', ('contains', )),
    ('premium', ('now', 'null')),
)


flat_available_filters = frozenset(sum([
    ['%s_%s' % (field, p) for p in predicts]
    for field, predicts in available_filters
], []))


def get_index(
    sex='',
    status='',
    birth_year='',
    premium='',
    fname='',
    sname='',
    country='',
    city='',
    phone='',
    interests='',
    domain='',
    **kwargs
):
    return '.'.join([
        sex,
        status,
        birth_year,
        premium,
        fname,
        sname,
        country,
        city,
        phone,
        interests,
        domain
    ])


@njit
def _filter_with_sex_eq(s, integers, s1, limit):
    _r = np.zeros(limit)
    _i = 0
    for i in range(s.shape[0]):
        if integers[s[i]][1] == s1:
            _r[_i] = s[i]
            if _r[-1]:
                break
            _i += 1
    return _r.astype(np.uint32)


@njit
def _filter_with_status_eq(s, integers, s1, limit):
    _r = np.zeros(limit)
    _i = 0
    for i in range(s.shape[0]):
        if integers[s[i]][2] == s1:
            _r[_i] = s[i]
            if _r[-1]:
                break
            _i += 1
    return _r.astype(np.uint32)


@njit
def _filter_with_status_neq(s, integers, s1, limit):
    _r = np.zeros(limit)
    _i = 0
    s1 = s1 * -1
    for i in range(s.shape[0]):
        if integers[s[i]][2] != s1:
            _r[_i] = s[i]
            if _r[-1]:
                break
            _i += 1
    return _r.astype(np.uint32)


@njit
def one_lvl_any(arr):
    return np.sort(arr).astype(np.uint32)


@njit
def filteq(buff_, query_n, limit, length_arrays, shape_0, *arrs):
    result = np.zeros(limit, dtype=np.uint32)
    pos = 0
    for i in range(shape_0):
        if pos == limit:
            return result

        for j in range(length_arrays):
            if i >= arrs[j].shape[0]:
                continue
            idx = arrs[j][i]

            if buff_[idx][0] != query_n:
                buff_[idx][0] = query_n
                buff_[idx][1] = 1
                buff_[idx][2] = 0
                continue

            if buff_[idx][2] == 1:
                continue

            buff_[idx][1] += 1
            if buff_[idx][1] == length_arrays:
                buff_[idx][2] = 1
                result[pos] = idx
                pos += 1

    return result



# TODO: http://localhost:8000/accounts/filter/?limit=36&country_eq=%D0%98%D1%81%D0%BF%D0%B5%D0%B7%D0%B8%D1%8F&fname_any=%D0%9D%D0%B5%D0%BB%D0%BB%D0%B8%2C%D0%9A%D1%81%D0%B5%D0%BD%D0%B8%D1%8F%2C%D0%9C%D0%B8%D0%BB%D0%B5%D0%BD%D0%B0%2C%D0%9C%D0%B0%D1%80%D0%B8%D0%BD%D0%B0%2C%D0%9C%D0%B8%D0%BB%D0%B0%D0%BD%D0%B0%2C%D0%94%D0%B0%D1%80%D0%B8%D0%BD%D0%B0%2C%D0%90%D0%BB%D0%B8%D0%BD%D0%B0&query_id=70
#  вот этот запрос сука не правильный и не правилно работает any, вообще зря я их попую
#

@njit
def filter_with_any(
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


@njit
def filter_with_any_sex(
        integers,
        sex,
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                        and integers[iteration_buffer[check_i]][1] == sex
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


@njit
def filter_with_any_status_neq(
        integers,
        status,
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    status = status * -1
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                        and integers[iteration_buffer[check_i]][2] != status
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


@njit
def filter_with_any_status(
        integers,
        status,
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                        and integers[iteration_buffer[check_i]][2] == status
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


@njit
def filter_with_any_sex_status(
        integers,
        sex,
        status,
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                        and integers[iteration_buffer[check_i]][1] == sex
                        and integers[iteration_buffer[check_i]][2] == status
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


@njit
def filter_with_any_sex_status_neq(
        integers,
        sex,
        status,
        buffer,
        query,
        limit,
        length_any_arrays,
        length_arrays,
        max_index,
        *arrays
):
    result = np.zeros(limit, dtype=np.uint32)
    need_count = int(length_arrays - length_any_arrays)
    position = 0
    iteration_buffer = np.zeros(length_arrays, dtype=np.uint32)
    status = status * -1
    for i in range(max_index):

        for any_i in range(length_any_arrays):

            if i < arrays[any_i].shape[0]:
                current_index = arrays[any_i][i]
                iteration_buffer[any_i] = current_index
                buffer[current_index][3] = query

        for eq_i in range(length_any_arrays, length_arrays):

            if i < arrays[eq_i].shape[0]:
                current_index = arrays[eq_i][i]
                iteration_buffer[eq_i] = current_index

                if buffer[current_index][0] != query:
                    buffer[current_index][0] = query
                    buffer[current_index][1] = 1
                    buffer[current_index][2] = 1 if need_count == 1 else 0
                    continue

                if buffer[current_index][2] == 1:
                    continue

                buffer[current_index][1] = buffer[current_index][1] + 1
                if buffer[current_index][1] == need_count:
                    buffer[current_index][2] = 1

        for check_i in range(iteration_buffer.shape[0]):
            if iteration_buffer[check_i]:
                check_buffer = buffer[iteration_buffer[check_i]]
                if (
                        check_buffer[0] == query
                        and check_buffer[3] == query
                        and check_buffer[2] == 1
                        and integers[iteration_buffer[check_i]][1] == sex
                        and integers[iteration_buffer[check_i]][2] != status
                ):
                    result[position] = iteration_buffer[check_i]
                    position = position + 1
                iteration_buffer[check_i] = 0

        if position >= limit:
            return result

    return result


ss_with_any_map = {
    'sex_eq': filter_with_any_sex,
    'status_eq': filter_with_any_sex_status,
    'status_neq': filter_with_any_sex_status_neq
}


map_status_sex_func = {
    'sex_eq': _filter_with_sex_eq,
    'status_eq': _filter_with_status_eq,
    'status_neq': _filter_with_status_neq,
}

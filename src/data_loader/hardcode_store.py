"""
Так у нас есть 4 уровня фильтров. Получаются следующий варианты.
1 - Уровень, мы берем всегда из первичного индекса или ищем по маске. Первый индекс
может быть также и на всякие lt, gt, start. Вариант когда мы не берем из первичного
сразу это contains и any. Тут придется еще поискать, с contains можно брать наименьший
и проверять по нему в первую очередь.
2 - Уровень, первым делом мы проверяем, а есть ли у нас маска и вторичный индекс, если
есть или то или то, то мы можем сразу смело отдать результат. Если нет должны выбрать
наименьший индекс и дальше идти фильтровать
3 - Уровень тут всегда есть маска и можем проверить наличие вторичного индекса, скорее
всего он должен быть.
4 - Тут будет один из вариантов второго уровня, плюс всегда есть маска

Данные по группам

{'city': True,
 'country': True,
 'likes': True,
 'joined': True,
 'status': True,
 'birth': True,
 'interests': True}


"""


import os
import logging
import numpy as np
import ujson
import urllib.parse
from util import (
    flat_available_filters,
    get_index,
    map_status_sex_func,
    one_lvl_any,
    filteq,
    filter_with_any,
    filter_with_any_sex,
    filter_with_any_status,
    filter_with_any_status_neq,
    filter_with_any_sex_status,
    filter_with_any_sex_status_neq,
)

from typing import TYPE_CHECKING, Optional, Dict, Tuple, List, Callable
from methods import (
    equal_one_with_mask,
    two_equal_jit
)

from data_loader.func_generators import two_lvl_index_two_args

if TYPE_CHECKING:
    from numpy import ndarray

JsonData = Optional[Dict[str, int]]
NumpyData = Optional['ndarray']


top_id = 0
_path = './prepared'
_init_counter = [1]

_city_json: JsonData = None
_city_npy: NumpyData = None

_country_json: JsonData = None
_country_npy: NumpyData = None

_fname_json: JsonData = None
_fname_npy: NumpyData = None

_sname_json: JsonData = None
_sname_npy: NumpyData = None

_interests_json: JsonData = None
_interests_npy: NumpyData = None

_domain_json: JsonData = None
_domain_npy: NumpyData = None

_integers_npy: NumpyData = None
_emails_npy: NumpyData = None
_set_emails = None

_sex_json = {
    'm': ord('m'),
    'f': ord('f'),
}
_sex_npy = np.zeros(110, dtype=np.str)
_sex_npy[ord('m')] = 'm'
_sex_npy[ord('f')] = 'f'

_status_json = {
    'свободны': 1,
    'заняты': 2,
    'всё сложно': 3
}
_status_npy = np.array(['', 'свободны', 'заняты', 'всё сложно'])

_top_lvl_indexes: Dict[str, NumpyData] = {}
_strings_map: Dict[str, List[Tuple[str, int, NumpyData]]] = None
prepared_func: Callable = None
_buffer: NumpyData = None


field_to_index = {
    'email': 'domain',
    'birth': 'birth_year'
}


def init(load_path):
    global _path
    _path = load_path


level_2_filter_list = [
 frozenset({'sex', 'status'}),
 frozenset({'birth_year', 'sex'}),
 frozenset({'premium', 'sex'}),
 frozenset({'premium', 'status'}),
 # frozenset({'fname', 'sex'}),
 # frozenset({'sex', 'sname'}),
 # frozenset({'country', 'sex'}),
 # frozenset({'city', 'sex'}),
 # frozenset({'phone', 'sex'}),
 # frozenset({'interests', 'sex'}),
 # frozenset({'birth', 'status'}),
 # frozenset({'country', 'status'}),
 # frozenset({'city', 'status'}),
 # frozenset({'interests', 'status'}),
 frozenset({'birth_year', 'premium'}),
 frozenset({'birth_year', 'country'}),
 # frozenset({'birth', 'city'}),
 frozenset({'interests', 'premium'}),
 frozenset({'country', 'fname'}),
 frozenset({'city', 'fname'}),
 # frozenset({'country', 'sname'}),
 # frozenset({'city', 'sname'}),
 frozenset({'country', 'phone'}),
 frozenset({'country', 'interests'}),
 frozenset({'city', 'phone'}),
 frozenset({'city', 'interests'}),
 frozenset({'country', 'domain'}),
]


def _create_index_level_2():
    count = 0
    keys = _top_lvl_indexes.keys()
    logging.warning('top lvl keys %s' % len(keys))
    for j in level_2_filter_list:
        i = list(j)
        kl1 = len(i[0])
        kl2 = len(i[1])

        keys1 = list(filter(lambda x: x.startswith(i[0]), keys))
        keys2 = list(filter(lambda x: x.startswith(i[1]), keys))
        for k1 in keys1:
            for k2 in keys2:
                _top_lvl_indexes[get_index(**{
                    k1[:kl1]: k1[kl1:],
                    k2[:kl2]: k2[kl2:],
                })] = np.intersect1d(_top_lvl_indexes[k1], _top_lvl_indexes[k2], assume_unique=True)[::-1]
                count += 1
    logging.warning('2 lvl keys %s' % count)


def _load_indexes():
    global _top_lvl_indexes
    for k, v in np.load(os.path.join(_path, 'indexes.npz')).items():
        _top_lvl_indexes[k] = v
    _create_index_level_2()


def load():
    _globals = globals()
    file_names = list(filter(lambda x: x.split('.')[-1] in ['json', 'npy'], os.listdir(_path)))
    logging.warning('Loading data files %s', file_names)
    for fn in file_names:
        name, extension = fn.split('.')
        global_name = '_%s_%s' % (name, extension)
        if global_name in _globals:
            full_path = os.path.join(_path, fn)
            if fn.endswith('json'):
                with open(full_path) as fd:
                    _globals[global_name] = ujson.load(fd)
            elif fn.endswith('npy'):
                _globals[global_name] = np.load(full_path)

    _globals['_set_emails'] = set(_emails_npy)

    global top_id
    top_id = _integers_npy[-1][0]
    _load_indexes()
    _globals['_buffer'] = np.zeros((_integers_npy.shape[0], 4), dtype=np.uint32)


def any_get_integers(base_name, integers_json, v):
    if isinstance(v, str):
        v = v.split(',')
    if len(v) == 1 and ',' in v[0]:
        v = v[0].split(',')
    arrays = []
    for i in v:
        try:
            arrays.append(
                _top_lvl_indexes[
                    '%s%s' % (base_name, integers_json[i])
                ])
        except KeyError:
            pass
    return arrays


def get_string_index(base_name, integers_json, v):
    try:
        return _top_lvl_indexes[
            '%s%s' % (base_name, integers_json[v])
        ]
    except KeyError:
        return np.array([])


def get_integer_index(base_name, v):
    try:
        return _top_lvl_indexes[
            '%s%s' % (base_name, v)
        ]
    except KeyError:
        return np.array([])


def get_two_2_lvl_index(*items):
    try:
        return _top_lvl_indexes[get_index(**{i[0]:str(i[1]) for i in items})]
    except KeyError:
        return np.array([])


filters_by_kwargs = {}


def filter_register(func):
    names = func.__name__[1:].split('__')
    filters_by_kwargs[frozenset(names)] = func

    def wrapper(*args, **kwargs):
        func(*args, **kwargs)
    return wrapper


def smart_filter_register(gen):
    def _(*names):
        func = gen(*names, globals_dict=globals())
        filters_by_kwargs[frozenset(names)] = func

        def wrapper(*args, **kwargs):
            func(*args, **kwargs)
        return wrapper
    return _


POS_FNAME = 9
POS_SNAME = 10
POS_CITY = 12
POS_COUNTRY = 11
POS_BIRTH = 3
POS_PREM_START = 6
POS_PREM_FINISH = 7
POS_STATUS = 2
POS_PHONE_CODE = 14
POS_PHONE = 13


# Serializers


def one_integer_premium_serializer(indexes, name, position, limit):
    return [{
        'id': int(_integers_npy[i][0]),
        'email': str(_emails_npy[i]),
        name: int(_integers_npy[i][position]),
        'premium': {
            'start': int(_integers_npy[i][POS_PREM_START]),
            'finish': int(_integers_npy[i][POS_PREM_FINISH])
        }
    } for i in indexes[:limit]]


def one_string_serializer(indexes, strings, name, position, limit):
    return [{
        'id': int(_integers_npy[i][0]),
        'email': str(_emails_npy[i]),
        name: strings[_integers_npy[i][position]],
    } for i in indexes[:limit]]


def one_string_phone_serializer(indexes, strings, name, position, limit):
    return [{
        'id': int(_integers_npy[i][0]),
        'email': str(_emails_npy[i]),
        'phone': '8(%s)%07d' % (_integers_npy[i][POS_PHONE_CODE], _integers_npy[i][POS_PHONE]),
        name: strings[_integers_npy[i][position]],
    } for i in indexes[:limit]]


def one_string_one_integer_serializer(indexes, strings, s_name, s_position, i_name, i_position, limit):
    return [{
        'id': int(_integers_npy[i][0]),
        'email': str(_emails_npy[i]),
        s_name: strings[_integers_npy[i][s_position]],
        i_name: int(_integers_npy[i][i_position]),
    } for i in indexes[:limit]]


def two_string_serializer(indexes, strings_1, s_name_1, s_position_1, strings_2, s_name_2, s_position_2, limit):
    return [{
        'id': int(_integers_npy[i][0]),
        'email': str(_emails_npy[i]),
        s_name_1: strings_1[_integers_npy[i][s_position_1]],
        s_name_2: strings_2[_integers_npy[i][s_position_2]],
    } for i in indexes[:limit]]


def _empty(*args, **kwargs):
    return []


def _fname_any(fname_any, limit, current_query):
    all_indexes = any_get_integers('fname', _fname_json, fname_any)
    indexes = np.hstack([i[:limit] for i in all_indexes])
    indexes = np.unique(one_lvl_any(indexes))[::-1]
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'fname': _fname_npy[_integers_npy[i][POS_FNAME]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'fname_any'})] = _fname_any


def _city_any(city_any, limit, current_query):
    all_indexes = any_get_integers('city', _city_json, city_any)
    indexes = np.hstack([i[:limit] for i in all_indexes])
    indexes = np.unique(one_lvl_any(indexes))[::-1]
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'city': _city_npy[_integers_npy[i][POS_CITY]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'city_any'})] = _city_any


def _interests_any(interests_any, limit, current_query):
    all_indexes = any_get_integers('interests', _interests_json, interests_any)
    indexes = np.hstack([i[:limit] for i in all_indexes])
    indexes = np.unique(one_lvl_any(indexes))[::-1]
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'interests_any'})] = _interests_any


def _city_eq(city_eq, limit, current_query):
    indexes = get_string_index('city', _city_json, city_eq)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'city': _city_npy[_integers_npy[i][POS_CITY]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'city_eq'})] = _city_eq


def _birth_year(birth_year, limit, current_query):
    indexes = get_integer_index('birth_year', birth_year)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'birth': int(_integers_npy[i][POS_BIRTH])
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'birth_year'})] = _birth_year


def _sex_eq(sex_eq, limit, current_query):
    indexes = get_integer_index('sex', _sex_json[sex_eq])
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'sex': sex_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'sex_eq'})] = _sex_eq


def _premium_null(premium_null, limit, current_query):
    indexes = get_integer_index('premium', '0' if premium_null == '1' else '-1')
    if premium_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'premium': {'start': int(_integers_npy[i][POS_PREM_START]), 'finish': int(_integers_npy[i][POS_PREM_FINISH])}
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                } for i in indexes[:limit]]

filters_by_kwargs[frozenset({'premium_null'})] = _premium_null


def _status_neq(status_neq, limit, current_query):
    indexes = get_integer_index('status', _status_json[status_neq] * -1)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'status': _status_npy[_integers_npy[i][POS_STATUS]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_neq'})] = _status_neq


def _premium_now(premium_now, limit, current_query):
    indexes = get_integer_index('premium', 2 if premium_now == '1' else 1)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
             'premium': {'start': int(_integers_npy[i][POS_PREM_START]), 'finish': int(_integers_npy[i][POS_PREM_FINISH])}
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'premium_now'})] = _premium_now


def _country_eq(country_eq, limit, current_query):
    indexes = get_string_index('country', _country_json, country_eq)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'country': _country_npy[_integers_npy[i][POS_COUNTRY]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_eq'})] = _country_eq


def _country_null(country_null, limit, current_query):
    indexes = get_integer_index('country', '0' if country_null == '1' else '-1')
    if country_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'country': _country_npy[_integers_npy[i][POS_COUNTRY]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_null'})] = _country_null


def _city_null(city_null, limit, current_query):
    indexes = get_integer_index('city', '0' if city_null == '1' else '-1')
    if city_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'city': _city_npy[_integers_npy[i][POS_CITY]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'city_null'})] = _city_null


def _sname_null(sname_null, limit, current_query):
    indexes = get_integer_index('sname', '0' if sname_null == '1' else '-1')
    if sname_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'sname': _sname_npy[_integers_npy[i][POS_SNAME]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'sname_null'})] = _sname_null


def _fname_null(fname_null, limit, current_query):
    indexes = get_integer_index('fname', '0' if fname_null == '1' else '-1')
    if fname_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'fname': _fname_npy[_integers_npy[i][POS_FNAME]]
                 } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'fname_null'})] = _fname_null


def _status_eq(status_eq, limit, current_query):
    indexes = get_integer_index('status', _status_json[status_eq])
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'status': status_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_eq'})] = _status_eq


def _phone_null(phone_null, limit, current_query):
    indexes = get_integer_index('phone', '0' if phone_null == '1' else '-1')
    if phone_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'phone': '8(%s)%07d' % (_integers_npy[i][POS_PHONE_CODE], _integers_npy[i][POS_PHONE]),
                 } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'phone_null'})] = _phone_null


def _phone_code(phone_code, limit, current_query):
    indexes = get_integer_index('phone', phone_code)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'phone': '8(%s)%07d' % (_integers_npy[i][POS_PHONE_CODE], _integers_npy[i][POS_PHONE]),
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'phone_code'})] = _phone_code


def _email_domain(email_domain, limit, current_query):
    indexes = get_integer_index('domain', _domain_json[email_domain])
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'email_domain'})] = _email_domain



filters_by_kwargs[frozenset({'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'likes_contains'})] = _empty
filters_by_kwargs[frozenset({'email_lt'})] = _empty
filters_by_kwargs[frozenset({'email_gt'})] = _empty
filters_by_kwargs[frozenset({'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'sname_starts'})] = _empty



def _status_eq_country_null(status_eq, country_null, limit, current_query):
    indexes = get_integer_index('country', '0' if country_null == '1' else '-1')
    indexes = equal_one_with_mask(('status_eq=%s' % status_eq,), _integers_npy, limit, indexes)
    if country_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
                'status': _status_npy[_integers_npy[i][POS_STATUS]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'status': _status_npy[_integers_npy[i][POS_STATUS]]
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_eq', 'country_null'})] = _status_eq_country_null


def _sex_eq_country_null(sex_eq, country_null, limit, current_query):
    indexes = get_integer_index('country', '0' if country_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    if country_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
                'sex': sex_eq
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'status': sex_eq
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_null', 'sex_eq'})] = _sex_eq_country_null


def _sex_eq_country_eq(sex_eq, country_eq, limit, current_query):
    indexes = get_string_index('country', _country_json, country_eq)
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
            'sex': sex_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_eq', 'sex_eq'})] = _sex_eq_country_eq


def _status_neq_city_null(status_neq, city_null, limit, current_query):
    indexes = get_integer_index('city', '0' if city_null == '1' else '-1')
    indexes = equal_one_with_mask(('status_neq=%s' % status_neq,), _integers_npy, limit, indexes)
    if city_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'city': _city_npy[_integers_npy[i][POS_CITY]],
                'status': _status_npy[_integers_npy[i][POS_STATUS]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'status': _status_npy[_integers_npy[i][POS_STATUS]]
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_neq', 'city_null'})] = _status_neq_city_null


def _sex_eq_city_null(sex_eq, city_null, limit, current_query):
    indexes = get_integer_index('city', '0' if city_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    if city_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'city': _city_npy[_integers_npy[i][POS_CITY]],
                'sex': sex_eq
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sex': sex_eq
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'city_null', 'sex_eq'})] = _sex_eq_city_null


def _status_neq_country_eq(status_neq, country_eq, limit, current_query):
    indexes = get_string_index('country', _country_json, country_eq)
    indexes = equal_one_with_mask(('status_neq=%s' % status_neq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
            'status': _status_npy[_integers_npy[i][POS_STATUS]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_eq', 'status_neq'})] = _status_neq_country_eq


def _sex_eq_email_domain(sex_eq, email_domain, limit, current_query):
    indexes = get_integer_index('domain', _domain_json[email_domain])
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
             'sex': sex_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'email_domain', 'sex_eq'})] = _sex_eq_email_domain


def _status_eq_city_eq(status_eq, city_eq, limit, current_query):
    indexes = get_string_index('city', _city_json, city_eq)
    indexes = equal_one_with_mask(('status_eq=%s' % status_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'city': _city_npy[_integers_npy[i][POS_CITY]],
            'status': _status_npy[_integers_npy[i][POS_STATUS]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_eq', 'city_eq'})] = _status_eq_city_eq


def _sex_eq_birth_year(sex_eq, birth_year, limit, current_query):
    indexes = get_integer_index('birth_year', birth_year)
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'birth': int(_integers_npy[i][POS_BIRTH]),
             'sex': sex_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'birth_year', 'sex_eq'})] = _sex_eq_birth_year


def _status_eq_city_null(status_eq, city_null, limit, current_query):
    indexes = get_integer_index('city', '0' if city_null == '1' else '-1')
    indexes = equal_one_with_mask(('status_eq=%s' % status_eq,), _integers_npy, limit, indexes)
    if city_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'city': _city_npy[_integers_npy[i][POS_CITY]],
                'status': _status_npy[_integers_npy[i][POS_STATUS]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'status': _status_npy[_integers_npy[i][POS_STATUS]]
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_eq', 'city_null'})] = _status_eq_city_null


def _sex_eq_premium_null(sex_eq, premium_null, limit, current_query):
    indexes = get_integer_index('premium', '0' if premium_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit, indexes)
    if premium_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'premium': {'start': int(_integers_npy[i][POS_PREM_START]), 'finish': int(_integers_npy[i][POS_PREM_FINISH])},
                 'sex': sex_eq
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sex': sex_eq
                } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'premium_null', 'sex_eq'})] = _sex_eq_premium_null


def _sex_eq_city_eq(sex_eq, city_eq, limit, current_query):
    indexes = get_string_index('city', _city_json, city_eq)
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
             'city': _city_npy[_integers_npy[i][POS_CITY]],
             'sex': sex_eq
             } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'sex_eq', 'city_eq'})] = _sex_eq_city_eq


def _sex_eq_phone_code(sex_eq, phone_code, limit, current_query):
    indexes = get_integer_index('phone', phone_code)
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'phone': '8(%s)%07d' % (_integers_npy[i][POS_PHONE_CODE], _integers_npy[i][POS_PHONE]),
             'sex': sex_eq
             } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'phone_code', 'sex_eq'})] = _sex_eq_phone_code


def _sex_eq_sname_null(sex_eq, sname_null, limit, current_query):
    indexes = get_integer_index('sname', '0' if sname_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    if sname_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sname': _sname_npy[_integers_npy[i][POS_SNAME]],
                 'sex': sex_eq
                 } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sex': sex_eq
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'sname_null', 'sex_eq'})] = _sex_eq_sname_null


def _sex_eq_fname_null(sex_eq, fname_null, limit, current_query):
    indexes = get_integer_index('fname', '0' if fname_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    if fname_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'fname': _fname_npy[_integers_npy[i][POS_FNAME]],
                 'sex': sex_eq
                 } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sex': sex_eq
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'fname_null', 'sex_eq'})] = _sex_eq_fname_null


def _status_neq_country_null(status_neq, country_null, limit, current_query):
    indexes = get_integer_index('country', '0' if country_null == '1' else '-1')
    indexes = equal_one_with_mask(('status_neq=%s' % status_neq,), _integers_npy, limit, indexes)
    if country_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
                'status': _status_npy[_integers_npy[i][POS_STATUS]]
                } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'status': _status_npy[_integers_npy[i][POS_STATUS]]
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_null', 'status_neq'})] = _status_neq_country_null


def _status_eq_birth_year(status_eq, birth_year, limit, current_query):
    indexes = get_integer_index('birth_year', birth_year)
    indexes = equal_one_with_mask(('status_eq=%s' % status_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'birth': int(_integers_npy[i][POS_BIRTH]),
             'status': status_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_eq', 'birth_year'})] = _status_eq_birth_year


def _sex_eq_premium_now(sex_eq, premium_now, limit, current_query):
    indexes = get_integer_index('premium', 2 if premium_now == '1' else 1)
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
             'premium': {'start': int(_integers_npy[i][POS_PREM_START]), 'finish': int(_integers_npy[i][POS_PREM_FINISH])},
             'sex': sex_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'premium_now', 'sex_eq'})] = _sex_eq_premium_now


def _status_neq_city_eq(status_neq, city_eq, limit, current_query):
    indexes = get_string_index('city', _city_json, city_eq)
    indexes = equal_one_with_mask(('status_neq=%s' % status_neq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'city': _city_npy[_integers_npy[i][POS_CITY]],
            'status': _status_npy[_integers_npy[i][POS_STATUS]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_neq', 'city_eq'})] = _status_neq_city_eq


def _status_neq_birth_year(status_neq, birth_year, limit, current_query):
    indexes = get_integer_index('birth_year', birth_year)
    indexes = equal_one_with_mask(('status_neq=%s' % status_neq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'birth': int(_integers_npy[i][POS_BIRTH]),
             'status': _status_npy[_integers_npy[i][POS_STATUS]]
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'status_neq', 'birth_year'})] = _status_neq_birth_year


def _status_eq_country_eq(status_eq, country_eq, limit, current_query):
    indexes = get_string_index('country', _country_json, country_eq)
    indexes = equal_one_with_mask(('status_eq=%s' % status_eq,), _integers_npy, limit, indexes)
    return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
            'country': _country_npy[_integers_npy[i][POS_COUNTRY]],
            'status': status_eq
            } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'country_eq', 'status_eq'})] = _status_eq_country_eq


def _sex_eq_phone_null(sex_eq, phone_null, limit, current_query):
    indexes = get_integer_index('phone', '0' if phone_null == '1' else '-1')
    indexes = equal_one_with_mask(('sex_eq=%s' % sex_eq,), _integers_npy, limit,
                                  indexes)
    if phone_null == '0':
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'phone': '8(%s)%07d' % (_integers_npy[i][POS_PHONE_CODE], _integers_npy[i][POS_PHONE]),
                 'sex': sex_eq
                 } for i in indexes[:limit]]
    else:
        return [{'id': int(_integers_npy[i][0]), 'email': str(_emails_npy[i]),
                 'sex': sex_eq
                 } for i in indexes[:limit]]
filters_by_kwargs[frozenset({'sex_eq', 'phone_null'})] = _sex_eq_phone_null


@filter_register
def _premium_now__birth_year(premium_now, birth_year, limit, current_query):
    premium = 2 if premium_now == '1' else 1
    indexes = get_two_2_lvl_index(('premium', premium), ('birth_year', birth_year))
    return one_integer_premium_serializer(indexes, 'birth', POS_BIRTH, limit)


@filter_register
def _country_eq__email_domain(country_eq, email_domain, limit, current_query):
    indexes = get_two_2_lvl_index(('country', _country_json[country_eq]), ('domain', _domain_json[email_domain]))
    return one_string_serializer(indexes, _country_npy, 'country', POS_COUNTRY, limit)


@filter_register
def _country_eq__birth_year(country_eq, birth_year, limit, current_query):
    indexes = get_two_2_lvl_index(('country', _country_json[country_eq]), ('birth_year', birth_year))
    return one_string_one_integer_serializer(indexes, _country_npy, 'country', POS_COUNTRY, 'birth', POS_BIRTH, limit)


smart_filter_register(two_lvl_index_two_args)('country_eq', 'phone_code')
smart_filter_register(two_lvl_index_two_args)('city_eq', 'phone_code')


# Setup

filters_by_kwargs[frozenset({'country_eq', 'sname_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'fname_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'fname_null'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'sname_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'city_null', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'email_domain'})] = _empty
filters_by_kwargs[frozenset({'birth_year', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_year', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'country_null'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'city_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'sex_eq'})] = _empty



filters_by_kwargs[frozenset({'premium_now', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'fname_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'likes_contains'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'likes_contains'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'fname_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'city_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'email_gt'})] = _empty
filters_by_kwargs[frozenset({'city_null', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'fname_any'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'likes_contains'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'email_gt'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'fname_any'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'sname_starts'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'fname_any', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'sname_starts'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'premium_null'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'fname_any', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'city_null'})] = _empty
# ----
filters_by_kwargs[frozenset({'interests_any', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_lt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'likes_contains', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'email_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'fname_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'fname_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'email_domain', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'sex_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'premium_now', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'email_domain', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_lt', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'fname_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'likes_contains', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'phone_code', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'premium_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'sex_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'status_neq', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'city_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'likes_contains', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'likes_contains', 'premium_null'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_gt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'status_neq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'sex_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'likes_contains', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'email_gt', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'sex_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'sex_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'likes_contains', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'birth_year', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'likes_contains', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'country_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'sname_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'sex_eq', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'email_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'email_domain', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'likes_contains', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_year', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'sname_starts', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'sname_null', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'country_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_any', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'city_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'status_neq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'fname_any', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_contains', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'sname_starts', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'likes_contains', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_neq', 'interests_contains'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'sname_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'interests_contains', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'fname_null', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'city_null', 'email_lt'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'fname_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_any', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'likes_contains', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'likes_contains', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'likes_contains', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_any', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_now', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_year', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'fname_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'phone_code', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'fname_any', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_year', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'city_any', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_gt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_any', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'city_null', 'sex_eq', 'phone_null'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'premium_null', 'likes_contains', 'birth_lt'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_lt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_gt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'likes_contains', 'birth_gt', 'premium_now'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'interests_contains', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_year', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_null', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt', 'status_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'country_null', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_gt'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_year', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'status_neq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt', 'status_neq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_lt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_year', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_year', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_year', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq', 'city_null'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any', 'birth_year'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'city_any'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'status_neq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'interests_any', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_lt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_year', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null', 'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_contains', 'status_neq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt', 'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_year', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_lt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_eq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_neq', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_eq', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_gt', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'interests_contains', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_null', 'birth_gt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_gt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_eq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'status_eq', 'interests_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'country_null', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'city_any', 'status_neq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any', 'interests_contains', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'sex_eq', 'status_neq', 'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_gt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'city_any', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'interests_any', 'status_neq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'country_null', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_year', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_gt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'status_neq', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'sex_eq', 'interests_contains', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'country_eq', 'birth_lt', 'status_eq', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_gt', 'status_neq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'country_null', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_year', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_year', 'sex_eq', 'city_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'city_any', 'birth_year', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_neq', 'birth_lt', 'city_any', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'birth_lt', 'status_neq', 'city_null', 'sex_eq'})] = _empty
filters_by_kwargs[frozenset({'status_eq', 'birth_lt', 'sex_eq', 'city_eq'})] = _empty


def f(filters, limit):
    current_query = _init_counter[0]
    _init_counter[0] += 1
    if filters:
        func = filters_by_kwargs.get(frozenset(filters.keys()), None)
        if func:
            return func(**filters, limit=limit, current_query=current_query)
        else:
            raise KeyError
    else:
        last_index = _integers_npy.shape[0] - 1
        return [
            {'id': int(_integers_npy[i][0]), 'email': _emails_npy[i]}
            for i in np.arange(last_index, last_index - limit, -1)
        ]


def validate_email(email):
    return (
        email
        and email not in _set_emails
    )


def validate_change_email(email):
    return (
        '@' in email
        and email not in _set_emails
    )


def update_top_id(id_):
    global top_id
    top_id = id_


def jit_prepare():
    # return
    with open('./data_loader/phase_1_get.ammo') as fn:
        for r in fn:
            try:
                if r.startswith('GET'):
                    query = r.split(' ')[1]
                    if not '/accounts/filter' in query:
                        continue
                    keys = urllib.parse.parse_qs(urllib.parse.urlparse(query).query)
                    limit = int(keys.pop('limit')[0])
                    keys.pop('query_id')
                    f({k: v if len(v) > 1 else v[0] for k, v in keys.items()}, limit)
            except Exception:
                pass

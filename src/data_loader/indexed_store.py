"""
Так у нас есть 4 уровня фильтров. Получаются следующий варианты.
1 - Уровень, мы берем всегда из первичного индекса или ищем по маске. Первый индекс
может быть также и на всякие lt, gt, start. Вариант когда мы не берем из первичного
сразу это contains и any. Тут придется еще поискать, с contains можно брать наименьший
и проверять по нему в первую очередь.
2 - Уровень, первым делом мы проверяем, а есть ли у нас маска и вторичный индекс, если
есть или то или то, то мы можем сразу смело отдать результат. Если нет должны выбрать
наименьший индекс и дальше идти фильтровать
3 - Уровень тут всегда есть маска и можем проверить наличие вторичного индекса, скорее
всего он должен быть.
4 - Тут будет один из вариантов второго уровня, плюс всегда есть маска

Данные по группам

{'city': True,
 'country': True,
 'likes': True,
 'joined': True,
 'status': True,
 'birth': True,
 'interests': True}


"""


import os
import logging
import numpy as np
import ujson
import urllib.parse
from util import (
    flat_available_filters,
    get_index,
    map_status_sex_func,
    one_lvl_any,
    filteq,
    filter_with_any,
    filter_with_any_sex,
    filter_with_any_status,
    filter_with_any_status_neq,
    filter_with_any_sex_status,
    filter_with_any_sex_status_neq,
)

from typing import TYPE_CHECKING, Optional, Dict, Tuple, List, Callable
from methods import (
    filter_equal,
    filter_equal_one_with_mask,
    filter_equal_with_mask,
    get_mask,
    get_any
)

if TYPE_CHECKING:
    from numpy import ndarray

JsonData = Optional[Dict[str, int]]
NumpyData = Optional['ndarray']


top_id = 0
_path = './prepared'
_init_counter = [1]

_city_json: JsonData = None
_city_npy: NumpyData = None

_country_json: JsonData = None
_country_npy: NumpyData = None

_fname_json: JsonData = None
_fname_npy: NumpyData = None

_sname_json: JsonData = None
_sname_npy: NumpyData = None

_interests_json: JsonData = None
_interests_npy: NumpyData = None

_domain_json: JsonData = None
_domain_npy: NumpyData = None

_integers_npy: NumpyData = None
_emails_npy: NumpyData = None
_set_emails = None

_sex_json = {
    'm': ord('m'),
    'f': ord('f'),
}
_sex_npy = np.zeros(110, dtype=np.str)
_sex_npy[ord('m')] = 'm'
_sex_npy[ord('f')] = 'f'

_status_json = {
    'свободны': 1,
    'заняты': 2,
    'всё сложно': 3
}
_status_npy = np.array(['', 'свободны', 'заняты', 'всё сложно'])

_top_lvl_indexes: Dict[str, NumpyData] = {}
_strings_map: Dict[str, List[Tuple[str, int, NumpyData]]] = None
prepared_func: Callable = None
_buffer: NumpyData = None


field_to_index = {
    'email': 'domain',
    'birth': 'birth_year'
}


def init(load_path):
    global _path
    _path = load_path


level_2_filter_list = [
 frozenset({'sex', 'status'}),
 frozenset({'birth', 'sex'}),
 frozenset({'premium', 'sex'}),
 frozenset({'premium', 'status'}),
 # frozenset({'fname', 'sex'}),
 # frozenset({'sex', 'sname'}),
 # frozenset({'country', 'sex'}),
 # frozenset({'city', 'sex'}),
 # frozenset({'phone', 'sex'}),
 # frozenset({'interests', 'sex'}),
 # frozenset({'birth', 'status'}),
 # frozenset({'country', 'status'}),
 # frozenset({'city', 'status'}),
 # frozenset({'interests', 'status'}),
 frozenset({'birth', 'premium'}),
 frozenset({'birth', 'country'}),
 # frozenset({'birth', 'city'}),
 frozenset({'interests', 'premium'}),
 frozenset({'country', 'fname'}),
 frozenset({'city', 'fname'}),
 # frozenset({'country', 'sname'}),
 # frozenset({'city', 'sname'}),
 frozenset({'country', 'phone'}),
 frozenset({'country', 'interests'}),
 frozenset({'city', 'phone'}),
 frozenset({'city', 'interests'}),
 frozenset({'country', 'domain'}),
]


def _create_index_level_2():
    count = 0
    keys = _top_lvl_indexes.keys()
    logging.warning('top lvl keys %s' % len(keys))
    for j in level_2_filter_list:
        i = list(j)
        kl1 = len(i[0])
        kl2 = len(i[1])

        keys1 = list(filter(lambda x: x.startswith(i[0]), keys))
        keys2 = list(filter(lambda x: x.startswith(i[1]), keys))
        for k1 in keys1:
            for k2 in keys2:
                _top_lvl_indexes[get_index(**{
                    k1[:kl1]: k1[kl1:],
                    k2[:kl2]: k2[kl2:],
                })] = np.intersect1d(_top_lvl_indexes[k1], _top_lvl_indexes[k2], assume_unique=True)[::-1]
                count += 1
    logging.warning('2 lvl keys %s' % count)


def _load_indexes():
    global _top_lvl_indexes
    for k, v in np.load(os.path.join(_path, 'indexes.npz')).items():
        _top_lvl_indexes[k] = v
    _create_index_level_2()


def _get_prepared_func():
    _args = [None]
    _globals = globals()

    def any_get_integers(_f, v):
        if isinstance(v, str):
            v = v.split(',')
        arrays = []
        for i in v:
            try:
                arrays.append(_globals['_%s_json' % _f][i])
            except KeyError:
                pass
        return arrays

    def get_year(_f, v):
        _ = _top_lvl_indexes['%s_year%s' % (_f, v)]
        return v

    _get_int = {
        'eq': lambda _f, v: _globals['_%s_json' % _f][v],
        'neq': lambda _f, v: _status_json[v] * -1,
        'year': get_year,
        'domain': lambda _f, v: _domain_json[v],
        'null': lambda _f, v: '0' if v == '1' else '-1',
        'code': lambda _f, v: v,
        'now': lambda _f, v: 2 if v == '1' else 1,
        'any': any_get_integers
    }

    def kwargs_eq(_f, v, k):
        k['values'][_f] = v

    def kwargs_neq(_f, v, k):
        k['strings_items'].append(('status', 2, _status_npy))

    def kwargs_year(_f, v, k):
        k['integers_items'].append(('birth', 3))

    def kwargs_domain(_f, v, k):
        pass

    def kwargs_null(_f, v, k):
        if v == '0':
            k['premium'] = k['premium'] or _f == 'premium'
            k['phone_code'] = k['phone_code'] or _f == 'phone'
            k['strings_items'].extend(_strings_map.get(_f, []))

    def kwargs_code(_f, v, k):
        k['phone_code'] = True

    def kwargs_now(_f, v, k):
        k['premium'] = True

    def kwargs_any(_f, v, k):
        k['strings_items'].extend(_strings_map.get(_f, []))

    _get_kwargs = {
        'eq': kwargs_eq,
        'neq': kwargs_neq,
        'year': kwargs_year,
        'domain': kwargs_domain,
        'null': kwargs_null,
        'code': kwargs_code,
        'now': kwargs_now,
        'any': kwargs_any
    }

    def _(items, kwargs):
        result = {}
        for k, v in items:
            field, predict = k.split('_')
            try:
                int_key = _get_int[predict](field, v)
            except KeyError as e:
                if field not in ['sex', 'status']:
                    raise StopIteration
                else:
                    raise e
            _get_kwargs[predict](field, v, kwargs),
            result[k] = [
                field,
                field_to_index.get(field, field),
                predict,
                int_key,
                k,
                v
            ]
        return result
    return _


def load():
    _globals = globals()
    file_names = list(filter(lambda x: x.split('.')[-1] in ['json', 'npy'], os.listdir(_path)))
    logging.warning('Loading data files %s', file_names)
    for fn in file_names:
        name, extension = fn.split('.')
        global_name = '_%s_%s' % (name, extension)
        if global_name in _globals:
            full_path = os.path.join(_path, fn)
            if fn.endswith('json'):
                with open(full_path) as fd:
                    _globals[global_name] = ujson.load(fd)
            elif fn.endswith('npy'):
                _globals[global_name] = np.load(full_path)

    _globals['_set_emails'] = set(_emails_npy)

    global top_id
    top_id = _integers_npy[-1][0]
    _load_indexes()
    _globals['_strings_map'] = {
        'city': [('city', 12, _city_npy)],
        'country': [('country', 11, _country_npy)],
        'fname': [('fname', 9, _fname_npy)],
        'sname': [('sname', 10, _sname_npy)],
    }
    _globals['prepared_func'] = _get_prepared_func()
    _globals['_buffer'] = np.zeros((_integers_npy.shape[0], 4), dtype=np.uint32)


def filter_serializer(
        indexes,
        values={},
        premium=False,
        phone_code=False,
        strings_items=[],
        integers_items=[]
):
    if not premium and not phone_code:
        return [
            {
                'id': int(_integers_npy[i][0]),
                'email': str(_emails_npy[i]),
                **values,
                **{n: str(arr[_integers_npy[i][pos]]) for n, pos, arr in strings_items},
                **{n: int(_integers_npy[i][pos]) for n, pos in integers_items}
            }
            for i in indexes
        ]
    elif premium and phone_code:
        return [
            {
                'id': int(_integers_npy[i][0]),
                'email': str(_emails_npy[i]),
                'premium': {'start': int(_integers_npy[i][6]), 'finish': int(_integers_npy[i][7])},
                'phone': '8(%s)%s' % (_integers_npy[i][14], _integers_npy[i][13]),
                **values,
                **{n: str(arr[_integers_npy[i][pos]]) for n, pos, arr in strings_items},
                **{n: int(_integers_npy[i][pos]) for n, pos in integers_items}
            }
            for i in indexes
        ]
    elif premium:
        return [
            {
                'id': int(_integers_npy[i][0]),
                'email': str(_emails_npy[i]),
                'premium': {'start': int(_integers_npy[i][6]), 'finish': int(_integers_npy[i][7])},
                **values,
                **{n: str(arr[_integers_npy[i][pos]]) for n, pos, arr in strings_items},
                **{n: int(_integers_npy[i][pos]) for n, pos in integers_items}

            }
            for i in indexes
        ]
    elif phone_code:
        return [
            {
                'id': int(_integers_npy[i][0]),
                'email': str(_emails_npy[i]),
                'phone': '8(%s)%07d' % (_integers_npy[i][14], _integers_npy[i][13]),
                **values,
                **{n: str(arr[_integers_npy[i][pos]]) for n, pos, arr in strings_items},
                **{n: int(_integers_npy[i][pos]) for n, pos in integers_items}

            }
            for i in indexes
        ]


def lvl_1_filter(_, limit, kwargs, current_query):
    _ = list(_.values())[0]
    if 'any' in _:
        any_indexes = np.hstack([_top_lvl_indexes['%s%s' % (_[1], i)][:limit] for i in _[3]])
        indexes = np.unique(one_lvl_any(
            any_indexes
        ))[::-1]
    else:
        indexes = _top_lvl_indexes['%s%s' % (_[1], _[3])]
    return filter_serializer(indexes[:limit], **kwargs)


def _get_sex_status_index(items):
    sex_status_index = -1, None
    for i in range(len(items)):
        if items[i][-2] in map_status_sex_func:
            return i, map_status_sex_func[items[i][-2]]
    return sex_status_index


def lvl_2_filter(items, limit, kwargs, current_query):
    lvl2_index = get_index(**{i[1]: str(i[3]) for i in list(items.values())})
    try:
        indexes = _top_lvl_indexes[lvl2_index][:limit]
        return filter_serializer(np.extract(indexes.astype(np.bool), indexes), **kwargs)
    except KeyError:
        pass

    mask = get_mask(items)

    if mask:
        current_item = list(items.values())[0]
        if 'any' in current_item:
            any_indexes = np.hstack(
                [_top_lvl_indexes['%s%s' % (current_item[1], i)][:256] for i in current_item[3]])
            indexes = np.unique(one_lvl_any(any_indexes))[::-1]
        else:
            indexes = _top_lvl_indexes['%s%s' % (current_item[1], current_item[3])]
        indexes = filter_equal_one_with_mask(
            _integers_npy,
            mask,
            limit,
            indexes.shape[0],
            indexes
        )
        return filter_serializer(
            np.extract(indexes.astype(np.bool), indexes),
            **kwargs
        )

    try:
        current_any = get_any(items)
        item = list(items.values())[0]
        if current_any:
            current_any = current_any[0]
            arrays = [
                _top_lvl_indexes['%s%s' % (item[1], item[3])],
                *[_top_lvl_indexes['%s%s' % (current_any[1], i)] for i in current_any[3]],
            ][::-1]
            max_shape = max([i.shape[0] for i in arrays])
            indexes = filter_with_any(
                _buffer,
                current_query,
                limit,
                len(arrays) - 1,
                len(arrays),
                max_shape,
                *arrays
            )
            indexes = np.sort(indexes)[::-1]
        else:
            arrays = [_top_lvl_indexes['%s%s' % (i[1], i[3])] for i in items.values()]
            max_shape = max([i.shape[0] for i in arrays])
            indexes = filter_equal(_buffer, current_query, limit, len(arrays), max_shape, *arrays)
        return filter_serializer(np.extract(indexes.astype(np.bool), indexes), **kwargs)
    except KeyError:
        return []


def lvl_3_filter(_, limit, kwargs, current_query):
    _ = list(_.values())
    sex_status_index, sex_status_func = _get_sex_status_index(_)

    if sex_status_index != -1:
        s_or_s = _.pop(sex_status_index)
        lvl2_index = get_index(**{i[1]: str(i[3]) for i in _})

        try:
            indexes = _top_lvl_indexes[lvl2_index]
            indexes = sex_status_func(indexes, _integers_npy, s_or_s[3], limit)
            return filter_serializer(
                np.extract(indexes.astype(np.bool), indexes),
                **kwargs
            )
        except KeyError:
            pass

        try:
            if 'any' in _[0]:
                arrays = [
                             _top_lvl_indexes['%s%s' % (_[1][1], _[1][3])],
                             *[_top_lvl_indexes['%s%s' % (_[0][1], i)] for i in
                               _[0][3]],
                         ][::-1]
                max_shape = max([i.shape[0] for i in arrays])
                if s_or_s[-2] == 'status_eq':
                    indexes = filter_with_any_status(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
                elif s_or_s[-2] == 'sex_eq':
                    indexes = filter_with_any_sex(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
                elif s_or_s[-2] == 'status_neq':
                    indexes = filter_with_any_status_neq(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
            elif 'any' in _[1]:
                arrays = [
                             _top_lvl_indexes['%s%s' % (_[0][1], _[0][3])],
                             *[_top_lvl_indexes['%s%s' % (_[1][1], i)] for i in
                               _[1][3]],
                         ][::-1]
                max_shape = max([i.shape[0] for i in arrays])
                if s_or_s[-2] == 'status_eq':
                    indexes = filter_with_any_status(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
                elif s_or_s[-2] == 'sex_eq':
                    indexes = filter_with_any_sex(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
                elif s_or_s[-2] == 'status_neq':
                    indexes = filter_with_any_status_neq(
                        _integers_npy,
                        s_or_s[3],
                        _buffer,
                        current_query,
                        limit,
                        len(arrays) - 1,
                        len(arrays),
                        max_shape,
                        *arrays
                    )
                    return filter_serializer(
                        np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
            else:
                arrays = [_top_lvl_indexes['%s%s' % (i[1], i[3])] for i in _]
                max_shape = max([i.shape[0] for i in arrays])
                indexes = filter_equal(_buffer, current_query, limit, len(arrays), max_shape,
                                 *arrays)

                indexes = sex_status_func(indexes, _integers_npy, s_or_s[3], limit)
                return filter_serializer(np.extract(indexes.astype(np.bool), indexes),
                                         **kwargs)
        except KeyError:
            return []

    return []


def lvl_4_filter(filters, limit, kwargs, current_query):
    return []


filters_array = [None, lvl_1_filter, lvl_2_filter, lvl_3_filter, lvl_4_filter]


def f(filters, limit):
    current_query = _init_counter[0]
    _init_counter[0] += 1
    if not flat_available_filters.issuperset(set(filters.keys())):
        raise KeyError()

    if filters:
        func = filters_array[len(filters)]
        kwargs = {
            'values': {},
            'premium': False,
            'phone_code': False,
            'strings_items': [],
            'integers_items': []
        }
        _ = prepared_func(filters.items(), kwargs)
        return func(_, limit, kwargs, current_query)
    else:
        last_index = _integers_npy.shape[0] - 1
        return [
            {'id': int(_integers_npy[i][0]), 'email': _emails_npy[i]}
            for i in np.arange(last_index, last_index - limit, -1)
        ]


def validate_email(email):
    return (
        email
        and email not in _set_emails
    )


def validate_change_email(email):
    return (
        '@' in email
        and email not in _set_emails
    )


def update_top_id(id_):
    global top_id
    top_id = id_


def jit_prepare():
    # return
    with open('./data_loader/phase_1_get.ammo') as fn:
        for r in fn:
            try:
                if r.startswith('GET'):
                    query = r.split(' ')[1]
                    if not '/accounts/filter' in query:
                        continue
                    keys = urllib.parse.parse_qs(urllib.parse.urlparse(query).query)
                    limit = int(keys.pop('limit')[0])
                    keys.pop('query_id')
                    f({k: v if len(v) > 1 else v[0] for k, v in keys.items()}, limit)
            except Exception:
                pass

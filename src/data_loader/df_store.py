import pandas as pd
import numpy as np
import os
import datetime
from collections import defaultdict
from json_to_csv import ACCOUNTS_FIELDS, LIKES_FIELDS, INTERESTS_FIELDS


def s(k):
    i, j = k.split(',')
    return int(i), int(j)


def get_names(func, postfix):
    name = func.__name__
    query_name = '%s_%s' % (name, postfix)
    return name, query_name


class DFStore:

    def __init__(self, load_path):
        self.load_path = load_path
        self.likes = {}
        self.interests = defaultdict(list)
        self.common = None

    def load(self):

        self.common = pd.read_csv(
            os.path.join(self.load_path, 'accounts.csv'),
            header=None,
            names=ACCOUNTS_FIELDS,
        )[::-1]

        # LIKES
        ldf = pd.read_csv(
            os.path.join(self.load_path, 'likes.csv'),
            header=None,
            names=LIKES_FIELDS,
        )

        for i, string in zip(ldf['id'], ldf['likes']):
            if isinstance(string, str):
                likes_array = sorted(
                    [s(k) for k in string.split(';')],
                    key=lambda x: x[1],
                    reverse=True
                )
                self.likes[i] = np.array(list(zip(*likes_array)))
        del ldf

        # INTEREST
        idf = pd.read_csv(
            os.path.join(self.load_path, 'interests.csv'),
            header=None,
            names=INTERESTS_FIELDS,
        )
        for i, string in zip(idf['id'], idf['string']):
            if isinstance(string, str):
                self.interests[frozenset(string.split(','))].append(i)
        del idf

    @staticmethod
    def get_result(fields, result, limit, order):
        if result is not None:
            return result.sort_values(by='id', ascending=order == 1)[:limit][fields].replace('', None).to_dict('records')
        else:
            return []

    @staticmethod
    def eq(func):
        def _(v, t):
            return v == t
        name, query_name = get_names(func, 'eq')
        return name, query_name, func, _

    @staticmethod
    def neq(func):
        def _(v, t):
            return v != t
        name, query_name = get_names(func, 'neq')
        return name, query_name, func, _

    @staticmethod
    def lt(func):
        def _(v, t):
            return v <= t
        name, query_name = get_names(func, 'lt')
        return name, query_name, func, _

    @staticmethod
    def gt(func):
        def _(v, t):
            return v >= t
        name, query_name = get_names(func, 'gt')
        return name, query_name, func, _

    @staticmethod
    def domain(func):
        def _(v, t):
            return v.split('@')[-1] == t
        name, query_name = get_names(func, 'domain')
        return name, query_name, func, _

    @staticmethod
    def year(func):
        def _(v, t):
            return datetime.datetime.fromtimestamp(v).year == t
        name, query_name = get_names(func, 'year')
        return name, query_name, func, _

    @staticmethod
    def null(func):
        def _(v, t):
            return isinstance(v, float) if t == '1' else not isinstance(v, float)
        name, query_name = get_names(func, 'null')
        return name, query_name, func, _

    @staticmethod
    def starts(func):
        def _(v, t):
            return not isinstance(v, float) and v.startswith(t)
        name, query_name = get_names(func, 'starts')
        return name, query_name, func, _

    @staticmethod
    def code(func):
        def _(v, t):
            return not isinstance(v, float) and int(v[2:5]) == int(t)
        name, query_name = get_names(func, 'code')
        return name, query_name, func, _

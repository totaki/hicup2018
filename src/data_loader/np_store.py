"""
strings.np
|sex|status|email|fname|sname|country|city

integers.np
|id|birth

db
|sex|status|birth_year|birth
"""
import os
import logging
import sys
import pandas as pd
import numpy as np
from itertools import combinations
from numba import njit, prange
from json_to_csv import ACCOUNTS_FIELDS
import ujson
from util import flat_available_filters

# NOT_FILL_NA = 'NOTFILLNA'
#
# to_sex = {
#     'm': 1,
#     'f': 2
# }
#
# from_sex = ('m', 'f')
#
#
# to_status = {
#     u'свободны': 1,
#     u'заняты': 2,
#     u'всё сложно': 3
# }
#
# from_status = (u'свободны', u'заняты', u'всё сложно')
#
#
# def create_np_from_df(df, fields, fill_na=NOT_FILL_NA, series=False):
#     if series:
#         current_array = np.array([i for i in df])
#     else:
#         if fill_na != NOT_FILL_NA:
#             current_df = df[fields].fillna(fill_na)
#         else:
#             current_df = df[fields]
#         current_array = np.array([i for i in zip(*[current_df[j] for j in fields])])
#     # Какого то хуя в джупитере тут когда делаем интеджеры
#     return np.append(
#         np.zeros((1, current_array.shape[1]), dtype=current_array.dtype),
#         current_array,
#         axis=0
#     )
#
#
# if sys.version_info[0] == 2:
#     def prepare_integers(r, **kwargs):
#         return [
#             to_sex[r['sex'].decode('utf-8')],
#             to_status[r['status'].decode('utf-8')]
#         ]
# else:
#     def prepare_integers(r, **kwargs):
#         return [
#             to_sex[r['sex']],
#             to_status[r['status']]
#         ]
#
#
# def unique_strings(df, strings):
#     dct_result = {}
#     for i in strings:
#         s = df[i]
#         dct_result.update(dict(zip(
#             set(s),
#             range(1, s.count() + 1)
#         )))
#     return dct_result
#
#
# def load(load_path):
#     # df = pd.read_csv(
#     #     os.path.join(load_path, 'accounts.csv'),
#     #     header=None,
#     #     names=ACCOUNTS_FIELDS,
#     # )[::-1]
#
#     integers = np.load(os.path.join(load_path, 'integers.npy'))
#     strings = np.load(os.path.join(load_path, 'strings.npy'))
#
#     return integers, strings
#
#
# with open(os.path.join(os.environ.get('DATA_PATH', './prepared'), 'strings.json')) as f:
#     unique_strings = ujson.load(f)
#
# # print(unique_strings)
#
# # Все индексы преобразований для цифровых фильтров должны начинаться с 1
# # при фильтрации по eq мы просто сравниваем, при neq мы (получается только для статуса)
# # для всех которые null это в нашем случае получается если null=1 то мы проверяем все кто 0, если null=0
# # всех у кого true
#
# def base_filter():
#     """
#     Базовый преобразователь полей должен возвращать
#     - название поля в отображающем массиве
#     - результирующую позицию отображающем массиве
#     - массив позиций в поиске
#     - массив сравнений в поиске
#     - массив позиций в greater
#     - массив сравнений в greater
#     """
#     pass
#
#
# base_filter.prefix = 'filter__'
#
#
# def filter__sex_eq(v):
#     return 'sex', 0, [0], [to_sex[v]], [], []
#
#
# def filter__status_eq(v):
#     return 'status', 1, [1], [to_status[v]], [], []
#
#
# available_filters = {
#     k[len(base_filter.prefix):]: v
#     for k, v in filter(lambda x: x[0].startswith(base_filter.prefix), locals().items())
# }
#
# fs = {
#     'sex_eq': lambda x: [[to_sex[x]], [0]],
#     'status_eq': lambda x: [[to_status[x]], [1]]
# }
#
# positions = {
#     'sex_eq': 0,
#     'status_eq': 2
# }
#
# names = {
#     'sex_eq': 'sex',
#     'status_eq': 'status',
# }
#
#
# fil1 = frozenset({'sex_eq'})
# fil2 = frozenset({'sex_eq', 'status_eq'})
# fil3 = frozenset({'status_eq'})
# fil4 = frozenset({'status_eq', 'country_eq'})
# fil5 = frozenset({'sex_eq', 'country_eq'})
# fil6 = frozenset({'sex_eq', 'status_eq', 'country_eq'})
# fil7 = frozenset({'country_eq'})
# fil_none = set()
#
#
# def f1(filters):
#     keys = set(filters.keys())
#     if keys == fil1:
#         return (
#             ['sex'],
#             [1],
#             [ord(filters['sex_eq'])]
#         )
#
#     elif keys == fil2:
#         return (
#             ['sex', 'status'],
#             [1, 2],
#             [ord(filters['sex_eq']), to_status[filters['status_eq']]]
#         )
#     elif keys == fil3:
#         return (
#             ['status'],
#             [2],
#             [to_status[filters['status_eq']]]
#         )
#     elif keys == fil4:
#         return (
#             ['status', 'country'],
#             [2, 5],
#             [to_status[filters['status_eq']], unique_strings[filters['country_eq']]]
#         )
#     elif keys == fil5:
#         return (
#             ['sex', 'country'],
#             [1, 5],
#             [ord(filters['sex_eq']), unique_strings[filters['country_eq']]]
#         )
#     elif keys == fil6:
#         return (
#             ['sex', 'status', 'country'],
#             [1, 2, 5],
#             [ord(filters['sex_eq']), to_status[filters['status_eq']], unique_strings[filters['country_eq']]]
#         )
#     elif keys == fil7:
#         return (
#             ['country'],
#             [5],
#             [unique_strings[filters['country_eq']]]
#         )
#     elif keys == fil_none:
#         return (
#             [],
#             [],
#             []
#         )
#     else:
#         raise KeyError('400')
#
#
# # http://0.0.0.0:8080/accounts/filter/?country_eq=%D0%98%D0%BD%D0%B4%D0%B8%D0%B7%D0%B8%D1%8F&limit=34&sex_eq=m
# def f2(fields, filtered, integers, strings):
#     if fields == ['sex']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'sex': chr(integers[i][1])
#             }
#             for i in filtered
#         ]
#     elif fields == ['sex', 'status']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'sex': chr(integers[i][1]),
#                 'status': strings[i][6]
#             }
#             for i in filtered
#         ]
#     elif fields == ['status']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'status': strings[i][6]
#             }
#             for i in filtered
#         ]
#     elif fields == ['status', 'country']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'status': strings[i][6],
#                 'country': strings[i][4]
#             }
#             for i in filtered
#         ]
#     elif fields == ['sex', 'country']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'sex': chr(integers[i][1]),
#                 'country': strings[i][4]
#             }
#             for i in filtered
#         ]
#     elif fields == ['sex', 'status', 'country']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'sex': chr(integers[i][1]),
#                 'status': strings[i][6],
#                 'country': strings[i][4]
#             }
#             for i in filtered
#         ]
#     elif fields == ['country']:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#                 'country': strings[i][4]
#             }
#             for i in filtered
#         ]
#     else:
#         return [
#             {
#                 'id': int(integers[i][0]),
#                 'email': strings[i][0],
#             }
#             for i in filtered
#         ]


@njit(parallel=True)
def _filter(s, eq, limit):
    _r = np.zeros(limit)
    _i = 0
    for i in prange(s.shape[0], 0, -1):
        if (eq == s[i]).all():
            _r[_i] = i
            if _r.all():
                break
            _i += 1
            pass
    return _r.astype(np.uint32)


def dummy_filter(v, eq, cols, fields):
    pass


def sex_eq(v, eq, cols, fields):
    eq.append(ord(v))
    cols.append(1)
    fields.append('sex')


m = {
    'sex_eq': sex_eq
}


def before(params):
    eq = []
    cols = []
    fields = []
    lst = list(params.items())
    for k, v in lst:
        m.get(k, dummy_filter)(v, eq, cols, fields)
    return eq, cols, fields


def after(i, fields, integers, emails, strings):
    r = {
        'id': int(integers[i][0]),
        'email': emails[i][0]
    }

    for f in fields:
        if f == 'sex':
            r['sex'] = chr(integers[i][1])
    return r


class NPStore(object):

    def __init__(self, load_path):
        self.load_path = load_path
        self.loaded = {}
        self.db = None
        self.df = None
        self.strings = None
        self.integers = None
        self.unique_strings = None
        self.len = None

    def load(self):
        file_names = list(filter(lambda x: x.split('.')[-1] in ['json', 'npy'], os.listdir(self.load_path)))
        logging.warning('Loading data files %s', file_names)
        for fn in file_names:
            full_path = os.path.join(self.load_path, fn)
            if fn.endswith('json'):
                with open(full_path) as fd:
                    self.loaded[fn] = ujson.load(fd)
            elif fn.endswith('npy'):
                    self.loaded[fn] = np.load(full_path)

    def f(self, filters, limit):
        """
        Надо получить
        - Список int колонок которые надо будет извлечь из базы
        - Список int значений для сравнения, пока только eq
        - Список int колонок которые потом надо будет извлеч из integers
        - Список int колонок которые потом надо будет извлеч из strings
        - Список названий колонок которые в итоге надо будет сериализовать
        """
        if not flat_available_filters.issuperset(set(filters.keys())):
            raise KeyError()

        loaded = self.loaded
        integers = loaded['integers.npy']
        emails = loaded['emails.npy']
        # OLD!!!!
        if filters:
            return []
        else:
            l = integers.shape[0] - 1
            return [
                {
                    'id': int(integers[i][0]),
                    'email': emails[i][0]
                }
                for i in np.arange(l, l - limit, -1)
            ]

        # strings = loaded['strings.json']
        #
        # if filters:
        #     eq, cols, fields = before(filters)
        #     if eq:
        #         iter_indexses = _filter(integers[:, np.array(cols)], np.array(eq), limit)
        #     else:
        #         iter_indexses = []
        # else:
        #     fields = []
        #     l = integers.shape[0] - 1
        #     iter_indexses = np.arange(l, l - limit, -1)
        #
        # return [
        #     after(i, fields, integers, emails, strings)
        #     for i in iter_indexses
        # ]

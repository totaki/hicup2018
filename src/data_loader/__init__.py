"""
То что хранится в числах
id, sex, status, birth, joined, premium_start, premium_finish, fname_i, sname_i, country_i, city_i, phone_code, phone_number

Уникальные строки
emails

Неуникальные строки
county, city, fname, sname, interests

Индексы
empty
country
city
"""

from data_loader.df_store import DFStore
from data_loader.np_store import NPStore
from data_loader import indexed_store
from data_loader import hardcode_store


def indexed_init(load_path):
    hardcode_store.init(load_path)
    return hardcode_store


Loader = indexed_init

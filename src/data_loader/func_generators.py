def two_lvl_index_two_args(arg1: str, arg2: str, globals_dict):
    fn = []
    arrays = ["def _({}, {}, limit, current_query):".format(arg1, arg2)]
    if arg1.endswith('eq') and arg2.endswith('code'):
        field, predict = arg1.split('_')
        arrays.extend([
            '    indexes = get_two_2_lvl_index(("{}", _{}_json[{}_eq]), ("phone", phone_code))'.format(field, field, field),
            '    return one_string_phone_serializer(indexes, _{}_npy, "{}", POS_{}, limit)'.format(field, field, field.upper()),
        ])
    globals_dict['fn'] = fn
    arrays.append('fn.append(_)')
    exec('\n'.join(arrays), globals_dict)
    return fn[0]
